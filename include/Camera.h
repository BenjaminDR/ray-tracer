#ifndef RAY_TRACER_CAMERA_H
#define RAY_TRACER_CAMERA_H

#include <Eigen/Core>

class Camera {

public:
    [[nodiscard]] virtual Ray generateRay(const Sample &sample) const = 0;
    virtual ~Camera() = default;
    [[nodiscard]] virtual Camera * create(int xResolution, int yResolution, Vector _origin, const Vector &lookat,
            const Vector &up, double fov) const = 0;
    [[nodiscard]] virtual Camera * clone() const = 0;

};


#endif //RAY_TRACER_CAMERA_H
