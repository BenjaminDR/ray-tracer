#ifndef RAY_TRACER_LIGHT_H
#define RAY_TRACER_LIGHT_H

#include <Base.h>
#include "film/RGBSpectrum.h"
#include <math/Ray.h>

class Light {

public:
    Light(const RGBSpectrum &c, double p) : color(c), power(p) {}
    virtual RGBSpectrum getRadiance(const Point &origin, const Normal &normal, Ray &shadowRay) = 0;
    [[nodiscard]] inline virtual RGBSpectrum Le() const {
        return color * power;
    }

    [[nodiscard]] virtual double pdf() const = 0;

    virtual ~Light() = default;

protected:
    const RGBSpectrum color;
    const double power;
};

#endif //RAY_TRACER_LIGHT_H
