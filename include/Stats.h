#ifndef RAY_TRACER_STATS_H
#define RAY_TRACER_STATS_H

#ifdef STATS
#include "atomic"

#define STAT_OP(var) var;

#define STAT_DEF(name) extern thread_local unsigned int name;
#define STAT_DEF_THREADED(name, name_threaded) \
    extern thread_local unsigned int name_threaded; \
    extern std::atomic<unsigned> name;

#define STAT_INIT(name) thread_local unsigned int name = 0;
#define STAT_INIT_THREADED(name, name_threaded) \
    thread_local unsigned int name_threaded = 0; \
    std::atomic<unsigned> name = 0;

#define STAT_END_THREADED(name, name_threaded) name += name_threaded;

#else
#define STAT_OP(var)
#define STAT_DEF(name)
#define STAT_DEF_THREADED(name, name_threaded)
#define STAT_INIT(name)
#define STAT_INIT_THREADED(name, name_threaded)
#define STAT_END_THREADED(name, name_threaded)
#endif

STAT_DEF_THREADED(BBintersects, BBintersects_l)
STAT_DEF_THREADED(Shapeintersects, Shapeintersects_l)

STAT_DEF(max_bvh_depth)
STAT_DEF(min_bvh_depth)

#endif //RAY_TRACER_STATS_H
