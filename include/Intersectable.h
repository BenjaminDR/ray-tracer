#ifndef RAY_TRACER_INTERSECTABLE_H
#define RAY_TRACER_INTERSECTABLE_H

#include "math/Ray.h"
#include "HitObject.h"

class Intersectable {

public:
    virtual bool intersect(const Ray &ray, HitObject &hitObject, double maxDist) const = 0;
    [[nodiscard]] virtual double distanceTo(const Ray &ray) const = 0;
    [[nodiscard]] virtual bool encloses(const Point &p) const = 0;
    [[nodiscard]] virtual double recursiveSurfaceArea() const = 0;
    [[nodiscard]] virtual int interSectionCount(const Ray &ray, int depth, int minDepth, int maxDepth) const;

};


#endif //RAY_TRACER_INTERSECTABLE_H
