#ifndef RAY_TRACER_LAMBERTIAN_H
#define RAY_TRACER_LAMBERTIAN_H

#include <brdf/BRDF.h>
#include "film/RGBSpectrum.h"

class Lambertian : public BRDF {

public:
    Lambertian(const RGBSpectrum &color, double reflectionCoefficient);
    [[nodiscard]] RGBSpectrum evaluate() const override;

private:
    const double reflectionCoefficient;

};


#endif //RAY_TRACER_LAMBERTIAN_H
