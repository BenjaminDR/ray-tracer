#ifndef RAY_TRACER_SHADEABLE_H
#define RAY_TRACER_SHADEABLE_H

#include "Light.h"
#include "Material.h"

class Shadeable {

public:
    [[nodiscard]] virtual const Light* getLight() const = 0;
    [[nodiscard]] virtual const Material* getMaterial() const = 0;

};

#endif //RAY_TRACER_SHADEABLE_H
