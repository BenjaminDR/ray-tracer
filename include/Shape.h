#ifndef RAY_TRACER_SHAPE_H
#define RAY_TRACER_SHAPE_H


#include "math/Ray.h"
#include "math/Transformation.h"
#include "acceleration/AABoundingBox.h"
#include <Base.h>
#include <HitObject.h>

#include <utility>
#include <memory>

class AABoundingBox;

class Shape {

public:
    Shape()= default;;
    explicit Shape(Transformation  transformation) : t(std::move(transformation)){}
    virtual ~Shape() = default;

    [[nodiscard]] virtual bool intersect(const Ray &ray, HitObject &hitObject) const = 0;

    [[nodiscard]] virtual const AABoundingBox& getBoundingBox() const = 0;

    virtual double surfaceArea() const = 0;
    virtual Point getRandomSurfacePoint(const Point &visibleFrom, Normal &normal) const = 0;

protected:
    Transformation t;
    mutable std::unique_ptr<AABoundingBox> AABB;


};


#endif //RAY_TRACER_SHAPE_H
