#ifndef RAY_TRACER_HITOBJECT_H
#define RAY_TRACER_HITOBJECT_H

#include "Shadeable.h"

struct HitObject {
    double distance = DINF;
    Point hitPoint;
    Vector normal;
    Vector2 uv;
    const Shadeable* shadeable;
    int bvhDepth = 0;
};

#endif //RAY_TRACER_HITOBJECT_H
