#ifndef RAY_TRACER_MATERIAL_H
#define RAY_TRACER_MATERIAL_H

#include "film/RGBSpectrum.h"
#include <Base.h>

class Scene;
struct HitObject;

class Material {
public:
    [[nodiscard]] virtual RGBSpectrum reflectance(const HitObject &hitObject) const  = 0;
};

#endif //RAY_TRACER_MATERIAL_H
