#ifndef RAY_TRACER_BASE_H
#define RAY_TRACER_BASE_H

#include <Eigen/Core>

using Point = Eigen::Vector3d;
using Point2d = Eigen::Vector2d;
using Vector = Eigen::Vector3d;
using Vector2 = Eigen::Vector2d;
using Normal = Eigen::Vector3d;
using Sample = Eigen::Vector2d;

const double EPSILON = 0.0000001;

const double DINF = std::numeric_limits<double>::max();

#endif //RAY_TRACER_BASE_H
