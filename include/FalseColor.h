#ifndef RAY_TRACER_FALSECOLOR_H
#define RAY_TRACER_FALSECOLOR_H


#include <Material.h>
#include <film/RGBSpectrum.h>
#include <shading/Shader.h>

class FalseColorShapePdf : public Shader {

public:
    explicit FalseColorShapePdf(Shape* s) : shape(s) {}
    [[nodiscard]] RGBSpectrum shade(const Scene &scene, const HitObject &hitObject, unsigned shadowRays) const override {
        RGBSpectrum shade(1, 0, 0);
        for(unsigned i = 0; i < 500; i++) {
            Normal n;
            Point p = shape->getRandomSurfacePoint(Vector(0, 5, 0), n);
            if((hitObject.hitPoint - p).norm() < 0.05) {
                shade = RGBSpectrum(0, 0, 1);
            }
        }
        return shade;
    }


private:
    Shape* shape;
};

class FalseColorMeshPdf : public Shader {

public:
    [[nodiscard]] RGBSpectrum shade(const Scene &scene, const HitObject &hitObject, unsigned shadowRays) const override {
        assert(mesh);
        RGBSpectrum shade(1, 1, 1);
        for(unsigned i = 0; i < 500; i++) {
            Normal n;
            Point p = mesh->getRandomSurfacePoint(Vector(0, 5, 0), n);
            if((hitObject.hitPoint - p).norm() < 0.05) {
//                shade = RGBSpectrum(0, 0, 1);
                return RGBSpectrum(abs(n.x()), abs(n.y()), abs(n.z()));
            }
        }
        return shade;
    }

    void addMesh(MeshInstance* m) {
        mesh = m;
    }

private:
    MeshInstance* mesh = nullptr;
};




#endif //RAY_TRACER_FALSECOLOR_H
