#!/bin/bash

printf "Meshes, Instancing\n"
for i in {0..1000..10}
    do
        inst=$(valgrind -q --tool=massif --massif-out-file=massif.out ./bin/compare_bvh Instancing-memory $i ; grep -B 3 "peak" massif.out | grep "mem_heap_B" | sed 's/mem_heap_B=//')
        soup=$(valgrind -q --tool=massif --massif-out-file=massif.out ./bin/compare_bvh TriangleSoup-memory $i ; grep -B 3 "peak" massif.out | grep "mem_heap_B" | sed 's/mem_heap_B=//')
        printf "$i, $inst, $soup\n"
    done
