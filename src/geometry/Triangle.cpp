#include "Triangle.h"

#include <utility>
#include <film/Sampler.h>

Triangle::Triangle(Transformation transformation, const Point& _a, const Point& _b, const Point& _c) :
    Shape(std::move(transformation)),
    a(_a), b(_b), c(_c),
    normal((_b - _a).cross(_c - _a).normalized()){}


bool Triangle::intersect(const Ray &ray, HitObject &hitObject) const {
    Ray tRay = t / ray;

    Vector edge1(b - a);
    Vector edge2(c - a);

    Vector h(tRay.direction.cross(edge2));

    double det = edge1.dot(h);

    if (det > -EPSILON && det < EPSILON) return false; // Ray is parallel to triangle

    double invDet = 1 / det;

    Vector s(tRay.origin - a);

    double beta = invDet * s.dot(h);
    if (beta < 0 || beta > 1) return false;

    Vector q(s.cross(edge1));

    double gamma = invDet * tRay.direction.dot(q);
    if (gamma < 0 || beta + gamma > 1) return false;

    double T = invDet * edge2.dot(q);

    if(T < EPSILON) return false;

    hitObject.distance = T;
    hitObject.normal = t.applyInverseNormal(normal);
    hitObject.hitPoint = ray.origin + hitObject.distance * ray.direction;

    return true;
}

const AABoundingBox &Triangle::getBoundingBox() const {
    if(AABB) return *AABB;

    Point origin = t.translatePoint(Point(0, 0, 0));
    Point v1 = t * a + origin;
    Point v2 = t * b + origin;
    Point v3 = t * c + origin;

    AABB = std::make_unique<AABoundingBox>(v1.cwiseMin(v2).cwiseMin(v3), v1.cwiseMax(v2).cwiseMax(v3));
    return *AABB;
}

double Triangle::surfaceArea() const {
    Vector scale = t.getScale();
    Vector edge1(scale.cwiseProduct(b - a));
    Vector edge2(scale.cwiseProduct(c - a));

    return 0.5 * edge1.cross(edge2).norm();
}

Point Triangle::getRandomSurfacePoint(const Point &visibleFrom, Normal &norm) const {

    // TODO: inverse normal if not visible

    Sample sample = Sampler::getRandomSample();
    double sqrtS1 = sqrt(sample.x());
    double alpha = 1 - sqrtS1;
    double beta = sqrtS1 * (1 - sample.y());
    double gamma = sqrtS1 * sample.y();

    Vector toP = a * alpha + b * beta + c * gamma;

    norm = t.applyInverseNormal(normal);

    return t.translatePoint(Point(0, 0, 0)) + t * toP;
}


