#ifndef RAY_TRACER_TRIANGLE_H
#define RAY_TRACER_TRIANGLE_H


#include "Shape.h"

class Triangle : public Shape {

public:
    Triangle(Transformation transformation, const Point& a, const Point& b, const Point& c);

    [[nodiscard]] bool intersect(const Ray &ray, HitObject &hitObject) const override;

    [[nodiscard]] const AABoundingBox& getBoundingBox() const override;

    double surfaceArea() const override;
    Point getRandomSurfacePoint(const Point &visibleFrom, Normal &normal) const override;

private:
    Point a, b, c;
    const Normal normal;
};


#endif //RAY_TRACER_TRIANGLE_H
