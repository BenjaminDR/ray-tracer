#ifndef RAY_TRACER_SPHERE_H
#define RAY_TRACER_SPHERE_H


#include "Shape.h"

class Sphere : public Shape {

public:

    explicit Sphere(Transformation transformation);

    [[nodiscard]] bool intersect(const Ray &ray, HitObject &hitObject) const override;

    [[nodiscard]] const AABoundingBox& getBoundingBox() const override;

    double surfaceArea() const override;
    Point getRandomSurfacePoint(const Point &visibleFrom, Normal &normal) const override;
};


#endif //RAY_TRACER_SPHERE_H
