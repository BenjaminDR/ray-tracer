#include "FlatTriangle.h"

#include <utility>
#include <film/Sampler.h>

FlatTriangle::FlatTriangle(const Point& _a, const Point& _b, const Point& _c) :
        Shape(Transformation()),
        a(_a), b(_b), c(_c),
        normal((_b - _a).cross(_c - _a).normalized()){}


bool FlatTriangle::intersect(const Ray &ray, HitObject &hitObject) const {
    Vector edge1(b - a);
    Vector edge2(c - a);

    Vector h(ray.direction.cross(edge2));

    double det = edge1.dot(h);

    if (det > -EPSILON && det < EPSILON) return false; // Ray is parallel to triangle

    double invDet = 1 / det;

    Vector s(ray.origin - a);

    double beta = invDet * s.dot(h);
    if (beta < 0 || beta > 1) return false;

    Vector q(s.cross(edge1));

    double gamma = invDet * ray.direction.dot(q);
    if (gamma < 0 || beta + gamma > 1) return false;

    double T = invDet * edge2.dot(q);

    if(T < EPSILON) return false;

    hitObject.distance = T;
    hitObject.normal = normal;
    hitObject.hitPoint = ray.origin + hitObject.distance * ray.direction;

    return true;
}

const AABoundingBox &FlatTriangle::getBoundingBox() const {
    if(AABB) return *AABB;

    Point v1 = a;
    Point v2 = b;
    Point v3 = c;

    AABB = std::make_unique<AABoundingBox>(v1.cwiseMin(v2).cwiseMin(v3), v1.cwiseMax(v2).cwiseMax(v3));
    return *AABB;
}

double FlatTriangle::surfaceArea() const {
    Vector edge1(b - a);
    Vector edge2(c - a);

    return 0.5 * edge1.cross(edge2).norm();
}

Point FlatTriangle::getRandomSurfacePoint(const Point &visibleFrom, Normal &norm) const {

    // TODO: inverse normal if not visible

    Sample sample = Sampler::getRandomSample();
    double sqrtS1 = sqrt(sample.x());
    double alpha = 1 - sqrtS1;
    double beta = sqrtS1 * (1 - sample.y());
    double gamma = sqrtS1 * sample.y();

    Vector toP = a * alpha + b * beta + c * gamma;

    norm = normal;

    return toP;
}


