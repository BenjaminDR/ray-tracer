#include "Sphere.h"

#include <utility>
#include <film/Sampler.h>

Sphere::Sphere(Transformation transformation) : Shape(std::move(transformation)) {}

bool Sphere::intersect(const Ray &ray, HitObject &hitObject) const {
    Ray transformedRay = t / ray;

    double a = transformedRay.direction.squaredNorm();
    double b = 2.0 * (transformedRay.direction.dot(transformedRay.origin));
    double c = transformedRay.origin.squaredNorm() - 1.0;
    double d = b * b - 4.0 * a * c;

    if(d < 0) return false;

    double disc = sqrt(d);

    double q = -0.5 * (b < 0 ? (b - disc) : (b + disc));

    double t1 = c / q;
    if(t1 >= 0) {
        Point center = t.translatePoint(Point(0, 0, 0));

        hitObject.distance = t1;
        hitObject.hitPoint = ray.origin + ray.direction * hitObject.distance;
        hitObject.normal = (hitObject.hitPoint - center).normalized();
        return true;
    }

    double t0 = q / a;
    if(t0 >= 0) {
        Point center = t.translatePoint(Point(0, 0, 0));

        hitObject.distance = t0;
        hitObject.hitPoint = ray.origin + ray.direction * hitObject.distance;
        hitObject.normal = (hitObject.hitPoint - center).normalized();
        return true;
    }

    return false;
}

const AABoundingBox &Sphere::getBoundingBox() const {
    if(AABB) return *AABB;

    Eigen::Matrix4d M = t.get().matrix();
    double x = sqrt(pow(M(0,0), 2) + pow(M(0,1), 2) + pow(M(0,2), 2));
    double y = sqrt(pow(M(1,0), 2) + pow(M(1,1), 2) + pow(M(1,2), 2));
    double z = sqrt(pow(M(2,0), 2) + pow(M(2,1), 2) + pow(M(2,2), 2));

    Vector p(M(0,3), M(1, 3), M(2, 3));
    Vector q(x, y, z);

    AABB = std::make_unique<AABoundingBox>(p - q, p + q);
    return *AABB;
}

double Sphere::surfaceArea() const {
    Vector scale = t.getScale().cwiseAbs();
    if(scale.x() == scale.y() == scale.z()) return 4 * M_PI * scale.x() * scale.x();

    // Approximation of ellipsoid surface area
    double p = 8.0/5;
    return 4 * M_PI * pow(scale.cwiseProduct(Vector(scale.y(), scale.z(), scale.x()))
         .array().pow(p).sum() / 3, 1/p);
}

Point Sphere::getRandomSurfacePoint(const Point &visibleFrom, Normal &normal) const {
    Point tVisibleFrom = t.inverseTranslatePoint(Point(0, 0, 0)) + t / visibleFrom;

    Sample sample = Sampler::getRandomSample().cwiseProduct(Vector2(M_PI, 2 * M_PI));
    Vector p(sin(sample.x()) * sin(sample.y()), cos(sample.x()), sin(sample.x()) * cos(sample.y()));

    if(tVisibleFrom.dot(p) < 0) {
        normal = t.applyNormal(-p.normalized());
        return t.translatePoint(Point(0, 0, 0)) + t * (-p);
    }
    normal = t.applyNormal(p.normalized());
    return t.translatePoint(Point(0, 0, 0)) + t * p;
}
