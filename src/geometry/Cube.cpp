#include "Cube.h"

#include <utility>
#include <film/Sampler.h>

Cube::Cube(Transformation transformation) : Shape(std::move(transformation)) {}

// Very ugly for sake of optimization
bool Cube::intersect(const Ray &ray, HitObject &hitObject) const {
    // Cube indices
    double x0 = -1;
    double y0 = -1;
    double z0 = -1;

    double x1 = 1;
    double y1 = 1;
    double z1 = 1;

    Ray tRay = t / ray;

    Vector invDir = tRay.direction.cwiseInverse();

    double tMin, tMax, canMin, canMax;

    double signXInvDir = (invDir.x() >= 0) - (invDir.x() < 0);
    double signYInvDir = (invDir.y() >= 0) - (invDir.y() < 0);
    double signZInvDir = (invDir.z() >= 0) - (invDir.z() < 0);

    Normal normalMin(-signXInvDir, 0, 0);
    Normal normalMax(-signXInvDir, 0, 0);

    // x-axis
    if(invDir.x() >= 0) {
        tMin = (x0 - tRay.origin.x()) * invDir.x();
        tMax = (x1 - tRay.origin.x()) * invDir.x();
    } else {
        tMin = (x1 - tRay.origin.x()) * invDir.x();
        tMax = (x0 - tRay.origin.x()) * invDir.x();
    }

    // y-axis + early abort
    if(invDir.y() >= 0) {
        canMin = (y0 - tRay.origin.y()) * invDir.y();
        if(canMin > tMax) return false;
        canMax = (y1 - tRay.origin.y()) * invDir.y();
    } else {
        canMin = (y1 - tRay.origin.y()) * invDir.y();
        if(canMin > tMax) return false;
        canMax = (y0 - tRay.origin.y()) * invDir.y();
    }
    if(tMin > canMax) return false;

    // process candidates
    if(canMin > tMin) {
        tMin = canMin;
        normalMin = Normal(0, -signYInvDir, 0);
    }
    if(canMax < tMax) {
        tMax = canMax;
        normalMax = Normal(0, -signYInvDir, 0);
    }


    // z-axis + early abort
    if(invDir.z() >= 0) {
        canMin = (z0 - tRay.origin.z()) * invDir.z();
        if(canMin > tMax) return false;
        canMax = (z1 - tRay.origin.z()) * invDir.z();
    } else {
        canMin = (z1 - tRay.origin.z()) * invDir.z();
        if(canMin > tMax) return false;
        canMax = (z0 - tRay.origin.z()) * invDir.z();
    }
    if(tMin > canMax) return false;

    // process candidates
    if(canMin > tMin) {
        tMin = canMin;
        normalMin = Normal(0, 0, -signZInvDir);
    }
    if(canMax < tMax) {
        tMax = canMax;
        normalMax = Normal(0, 0, -signZInvDir);
    }

    // return intersection info
    if(tMin > EPSILON) {
        hitObject.distance = tMin;
        hitObject.normal = t.applyInverseNormal(normalMin);
    } else if(tMax > EPSILON) {
        hitObject.distance = tMax;
        hitObject.normal = t.applyInverseNormal(normalMax);
    } else return false;
    hitObject.hitPoint = ray.origin + ray.direction * hitObject.distance;

    return true;
}

const AABoundingBox &Cube::getBoundingBox() const {
    if(AABB) return *AABB;

    Point origin = t.translatePoint(Point(0, 0, 0));
    Vector cornerDir1 = t * Vector(1, 1, 1);
    Vector cornerDir2 = t * Vector(1, 1, -1);

    Point minCorner = origin + cornerDir1.cwiseMin(-cornerDir1).cwiseMin(cornerDir2).cwiseMin(-cornerDir2);
    Point maxCorner = origin + cornerDir1.cwiseMax(-cornerDir1).cwiseMax(cornerDir2).cwiseMax(-cornerDir2);

    AABB = std::make_unique<AABoundingBox>(minCorner, maxCorner);

    return *AABB;
}

double Cube::surfaceArea() const {
    Vector scale = t.getScale();
    return 2 * (scale.x() * scale.y() + scale.y() * scale.z() + scale.z() * scale.x());
}

Point Cube::getRandomSurfacePoint(const Point &visibleFrom, Normal &normal) const {
    Point tVisibleFrom = t.inverseTranslatePoint(Point(0, 0, 0)) + t / visibleFrom;

    Sample sample = Sampler::getRandomSample();
    Point p;
    Normal n;

    Vector scale = t.getScale();
    double xy = scale.x() * scale.y();
    double yz = scale.y() * scale.z();
    double zx = scale.z() * scale.x();
    double sum = xy + yz + zx;

    if(sample.x() < yz/sum) { // yz-face
        p = Point(1, -1 + sample.y() * 2, -1 + 2 * sum / yz * sample.x());
        n = Normal(1, 0, 0);
    }
    else if(sample.x() > (yz + zx)/sum) { // xy-face
        p = Point(-1 + 2 * sum / xy * (sample.x() - (yz + zx)/sum), -1 + 2 * sample.y(), 1);
        n = Normal(0, 0, 1);
    }
    else { // zx-face
        p = Point(-1 + 2 * sum / zx * (sample.x() - yz/sum), 1, -1 + 2 * sample.y());
        n = Normal(0, 1, 0);
    }

    if(tVisibleFrom.dot(p) < 0) {
        normal = t.applyNormal(-n);
        return t.translatePoint(Point(0, 0, 0)) + t * (-p);
    }
    normal = t.applyNormal(n);
    return t.translatePoint(Point(0, 0, 0)) + t * p;
}
