#ifndef RAY_TRACER_CUBE_H
#define RAY_TRACER_CUBE_H


#include "Shape.h"

class Cube : public Shape {

public:
    explicit Cube(Transformation transformation);

    [[nodiscard]] bool intersect(const Ray &ray, HitObject &hitObject) const override;

    [[nodiscard]] const AABoundingBox& getBoundingBox() const override;

    double surfaceArea() const override;
    Point getRandomSurfacePoint(const Point &visibleFrom, Normal &normal) const override;
};


#endif //RAY_TRACER_CUBE_H
