#include <geometry/Sphere.h>
#include <geometry/Cube.h>
#include <material/Matte.h>
#include <geometry/Triangle.h>
#include <core/WaveFrontParser.h>
#include <light/PointLight.h>
#include <light/GlobalLight.h>
#include <mesh/MeshInstance.h>
#include <light/AreaLight.h>
#include <material/Textured.h>
#include <FalseColor.h>
#include "SceneBuilder.h"

using namespace std;

void SceneBuilder::createScene(Scene &scene, int width, int height, const string &sceneName) {
    if(sceneName == "DiffuseTest1") return createDiffuseTest1Scene(scene, width, height);
    if(sceneName == "Veach") return createVeachScene(scene, width, height);
    if(sceneName == "AreaLight1") return createAreaLight1Scene(scene, width, height);
    if(sceneName == "AreaLight2") return createAreaLight2Scene(scene, width, height);
    if(sceneName == "SphereFlake") return createSphereFlakeScene(scene, width, height);
    if(sceneName == "Sponza") return createSponzaScene(scene, width, height);
    if(sceneName == "Museum") return createMuseumScene(scene, width, height);
    else {
        cerr << "Cannot find Scene '" << sceneName << "'\nExiting...\n";
        exit(1);
    }
}

void SceneBuilder::createDiffuseTest1Scene(Scene &scene, int width, int height) {
    // Camera
    Vector origin(0, 5, 0);
    Vector destination(0, 0, -20);
    Vector lookup(0, 1, 0);
    double fov = 90;

    scene.setCamera(new PerspectiveCamera(width, height, origin, destination - origin, lookup, fov));

    Transformation t1, t2, t3, t4, t5;
    t1.scale(5, 5, 5);
    t1.translate(Vector(-2, 5, -12));

    t2.scale(3, 1.5, 1);
    t2.rotateY(20);
    t2.translate(Vector(2, 1, -6));

    t3.scale(2, 2, 1);
    t3.rotateY(20);
    t3.translate(Vector(2, 2, -3));

    t4.scale(100, 100, 100);

    t5.scale(2, 2, 2);
    t5.rotateY(20);
    t5.translate(Vector(2, 3, -6));

    scene.addPrimitive(make_shared<Primitive>(dynamic_pointer_cast<Material,
            Matte>(make_shared<Matte>(RGBSpectrum(1, 0, 0))),
            make_shared<Sphere>(t1)));
    scene.addPrimitive(make_shared<Primitive>(dynamic_pointer_cast<Material,
            Matte>(make_shared<Matte>(RGBSpectrum(0, 1, 1))),
            make_shared<Cube>(t2)));
    scene.addPrimitive(make_shared<Primitive>(dynamic_pointer_cast<Material,
            Matte>(make_shared<Matte>(RGBSpectrum(1, 0, 1))),
            make_shared<Triangle>(t3, Point(-0.5, 0, 0), Point(0.5, 0, 0),
                    Point(0, 1, 0))));

    MeshInstance* planeMesh = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("plane"), t4));
    shared_ptr<Material> yellowMatte = make_shared<Matte>(RGBSpectrum(1, 1, 0));
    planeMesh->addMaterial(yellowMatte);

    MeshInstance* venusMesh = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("venus"), t5));
    shared_ptr<Material> venusMatte = make_shared<Matte>(RGBSpectrum(0.9, 1, 0.9));
    venusMesh->addMaterial(venusMatte);

    scene.addLight(make_unique<PointLight>(Point(6, 10, 0),
            RGBSpectrum(1, 1, 1), 800));
    scene.addLight(make_unique<GlobalLight>(RGBSpectrum(1, 1, 1), 0.05));
}

void SceneBuilder::createVeachScene(Scene &scene, int width, int height) {
    // Camera
//    Vector origin(-20, 10, 20);
//    Vector destination(0, 10, 0);
    Vector origin(0, 10, 35);
    Vector destination(0, 10, 0);
    Vector lookup(0, 1, 0);
    double fov = 40;

    scene.setCamera(new PerspectiveCamera(width, height, origin, destination - origin, lookup, fov));

    Transformation t;

    t.scale(10, 10, 10);


    shared_ptr<Material> whiteMatte = make_shared<Matte>(RGBSpectrum(1));
    shared_ptr<Material> eggMatte = make_shared<Matte>(RGBSpectrum(0.2, 0.2, 1));
    shared_ptr<Material> greyMatte = make_shared<Matte>(RGBSpectrum(0.5));
    shared_ptr<Material> brownMatte = make_shared<Matte>(RGBSpectrum(0.6, 0.3, 0));

    MeshInstance* vBoxMesh = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("veach/box"), t));
    vBoxMesh->addMaterial(whiteMatte);

    MeshInstance* vEggMesh = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("veach/egg"), t));
    vEggMesh->addMaterial(eggMatte);

    MeshInstance* vEmitterMesh = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("veach/emitter"), t));
    vEmitterMesh->addMaterial(greyMatte);

    MeshInstance* vLampMesh = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("veach/lamp"), t));
    vLampMesh->addMaterial(greyMatte);

    MeshInstance* vTableMesh = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("veach/table"), t));
    vTableMesh->addMaterial(brownMatte);


    scene.addLight(make_unique<GlobalLight>(RGBSpectrum(1), 0.01));
    scene.addLight(make_unique<PointLight>(Point(7.9, 11.8, -0.9),
                                                RGBSpectrum(1, 0.8, 0), 500));
    scene.addLight(make_unique<PointLight>(Point(-6.6, 13.5, -1.9),
                                                RGBSpectrum(1), 500));
}

void SceneBuilder::createAreaLight1Scene(Scene &scene, int width, int height) {
    Vector origin(0, 6, 13);
    Vector destination(0, 6, 0);
    Vector lookup(0, 1, 0);
    double fov = 40;
//    Vector origin(0, 10, 13);
//    Vector destination(0, 6, 0);
//    Vector lookup(0, 1, 0);
//    double fov = 40;


    scene.setCamera(new PerspectiveCamera(width, height, origin, destination - origin, lookup, fov));

    Transformation t, t1, t2, t3, t4, t5, t6, t7, t8, t9;
    t.scale(5, 5, 5);

    MeshInstance* tableM = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("table"), t));
    shared_ptr<Material> woodText = make_shared<Textured>("wood6.jpg");
    tableM->addMaterial(woodText);

    // Bunnies

    t1.scale(0.5, 0.5, 0.5);
    t1.translate(Vector(0, 7, 0));

    t2.rotateY(40);
    t2.scale(1.0/2, 1.0/2, 1.0/2);
    t2.translate(Vector(0, 5.5, -2));
    t2.rotateY(60);

    t3.rotateY(40);
    t3.scale(1.0/2, 1.0/2, 1.0/2);
    t3.translate(Vector(0, 5.5, -2));
    t3.rotateY(150);

    t4.rotateY(40);
    t4.scale(1.0/2, 1.0/2, 1.0/2);
    t4.translate(Vector(0, 5.5, -2));
    t4.rotateY(240);

    t5.rotateY(40);
    t5.scale(1.0/2, 1.0/2, 1.0/2);
    t5.translate(Vector(0, 5.5, -2));
    t5.rotateY(330);

    t6.rotateY(40);
    t6.scale(1.0/2, 1.0/2, 1.0/2);
    t6.translate(Vector(0, 5.5, -3));
    t6.rotateY(115);

    t7.rotateY(40);
    t7.scale(1.0/2, 1.0/2, 1.0/2);
    t7.translate(Vector(0, 5.5, -3));
    t7.rotateY(195);

    t8.rotateY(40);
    t8.scale(1.0/2, 1.0/2, 1.0/2);
    t8.translate(Vector(0, 5.5, -3));
    t8.rotateY(285);

    t9.rotateY(40);
    t9.scale(1.0/2, 1.0/2, 1.0/2);
    t9.translate(Vector(0, 5.5, -3));
    t9.rotateY(15);

    shared_ptr<MeshData> bunnyMeshData = WaveFrontParser::importMesh("bunny_low");
    shared_ptr<Material> whiteMatte = make_shared<Matte>(RGBSpectrum(1));
    shared_ptr<Material> bunny1Text = make_shared<Textured>("bunny1.jpg");
    shared_ptr<Material> bunny2Text = make_shared<Textured>("bunny2.jpg");
    shared_ptr<Material> bunny3Text = make_shared<Textured>("bunny3.jpg");
    shared_ptr<Material> bunny4Text = make_shared<Textured>("bunny4.jpg");
    shared_ptr<Material> bunny5Text = make_shared<Textured>("bunny5.jpg");
    shared_ptr<Material> bunny6Text = make_shared<Textured>("bunny6.jpg");
    shared_ptr<Material> bunny7Text = make_shared<Textured>("bunny7.jpg");

//    MeshInstance* bunnyMesh1 = scene.addMesh(make_unique<MeshInstance>(bunnyMeshData, t2));
//    MeshInstance* bunnyMesh2 = scene.addMesh(make_unique<MeshInstance>(bunnyMeshData, t3));
    MeshInstance* bunnyMesh3 = scene.addMesh(make_unique<MeshInstance>(bunnyMeshData, t4));
//    MeshInstance* bunnyMesh4 = scene.addMesh(make_unique<MeshInstance>(bunnyMeshData, t5));
//    MeshInstance* bunnyMesh5 = scene.addMesh(make_unique<MeshInstance>(bunnyMeshData, t6));
//    MeshInstance* bunnyMesh6 = scene.addMesh(make_unique<MeshInstance>(bunnyMeshData, t7));
//    MeshInstance* bunnyMesh7 = scene.addMesh(make_unique<MeshInstance>(bunnyMeshData, t8));
//    MeshInstance* bunnyMesh8 = scene.addMesh(make_unique<MeshInstance>(bunnyMeshData, t9));

//    bunnyMesh1->addMaterial(bunny4Text);
//    bunnyMesh2->addMaterial(bunny1Text);
    bunnyMesh3->addMaterial(bunny6Text);
//    bunnyMesh4->addMaterial(bunny2Text);
//    bunnyMesh5->addMaterial(bunny1Text);
//    bunnyMesh6->addMaterial(bunny7Text);
//    bunnyMesh7->addMaterial(bunny4Text);
//    bunnyMesh8->addMaterial(bunny4Text);



    scene.addLight(make_unique<PointLight>(Point(4, 8, 20), RGBSpectrum(1), 90));


    // Carrot

    Transformation t10;

    t10.scale(0.1, 0.1, 0.1);
    t10.rotateZ(40);
    t10.rotateY(20);
    t10.translate(Vector(0, 7, 0));

    shared_ptr<MeshData> carrotMeshData = WaveFrontParser::importMesh("carrot/CarrotLow-Poly");

    shared_ptr<Material> orangeMatte = make_shared<Matte>(RGBSpectrum(100.0/255, 16.5/255, 0.0/255));
    MeshInstance* carrotMesh = scene.addMesh(make_unique<MeshInstance>(carrotMeshData, t10));
    carrotMesh->addMaterial(orangeMatte);

//    shared_ptr<FalseColorMeshPdf> falseColorPdf = make_shared<FalseColorMeshPdf>();
//    MeshInstance* carrotMesh = scene.addMesh(make_unique<MeshInstance>(carrotMeshData, falseColorPdf, t10));
//    falseColorPdf->addMesh(carrotMesh);


    unique_ptr<Light> carrotLight = make_unique<AreaLight>(RGBSpectrum(100.0/255, 16.5/255, 0), 20, nullptr, carrotMesh);
    Light* carrotL = scene.addLight(move(carrotLight));
    carrotMesh->addLight(carrotL);

//    scene.addLight(make_unique<GlobalLight>(RGBSpectrum(1), 0.1));
}

void SceneBuilder::createAreaLight2Scene(Scene &scene, int width, int height) {
    Vector origin(0, 0, 3);
    Vector destination(0, 0, 0);
    Vector lookup(0, 1, 0);
    double fov = 55;

    scene.setCamera(new PerspectiveCamera(width, height, origin, destination - origin, lookup, fov));

    Transformation t1, t2, t3, t4, t5, t6, t7;

    t1.rotateZ(90);
    t1.translate(Vector(-1, 0, 0));

    t2.rotateX(90);
    t2.translate(Vector(0, 0, -1));

    t3.rotateZ(90);
    t3.translate(Vector(1, 0, 0));

    t4.translate(Vector(0, -1, 0));

    t5.translate(Vector(0, 1, 0));

    t6.scale(2.0/3, 2.0/40, 2.0/3);
    t6.translate(Vector(0, 1 - 1.0/40, 0));

    t7.translate(Vector(0, -0.5, 0));

    shared_ptr<MeshData> planeMeshData = WaveFrontParser::importMesh("plane");
    shared_ptr<Material> greyMatte = make_shared<Matte>(RGBSpectrum(0.75, 0.75, 0.75));
    shared_ptr<Material> redMatte = make_shared<Matte>(RGBSpectrum(1, 0, 0));
    shared_ptr<Material> greenMatte = make_shared<Matte>(RGBSpectrum(0, 1, 0));
    shared_ptr<Material> blueMatte = make_shared<Matte>(RGBSpectrum(0, 0, 1));

    // Red plane
    MeshInstance* redPlaneMesh = scene.addMesh(make_unique<MeshInstance>(planeMeshData, t1));
    redPlaneMesh->addMaterial(redMatte);
    // Green plane
    MeshInstance* greenPlaneMesh = scene.addMesh(make_unique<MeshInstance>(planeMeshData, t2));
    greenPlaneMesh->addMaterial(greenMatte);
    // Blue plane
    MeshInstance* bluePlaneMesh = scene.addMesh(make_unique<MeshInstance>(planeMeshData, t3));
    bluePlaneMesh->addMaterial(blueMatte);
    // Bottom plane
    MeshInstance* bottomPlaneMesh = scene.addMesh(make_unique<MeshInstance>(planeMeshData, t4));
    bottomPlaneMesh->addMaterial(greyMatte);
    // Top plane
    MeshInstance* topPlaneMesh = scene.addMesh(make_unique<MeshInstance>(planeMeshData, t5));
    topPlaneMesh->addMaterial(greyMatte);


    // Rectangle light
    shared_ptr<Shape> cube = make_shared<Cube>(t6);
    unique_ptr<Light> cubeLight = make_unique<AreaLight>(RGBSpectrum(1), 10, cube.get());
    scene.addPrimitive(make_shared<Primitive>(dynamic_pointer_cast<Material,
            Matte>(make_shared<Matte>(RGBSpectrum(1))), cube, cubeLight.get()));
//    scene.addPrimitive(make_shared<Primitive>(dynamic_pointer_cast<Material,
//            FalseColor>(make_shared<FalseColor>(RGBSpectrum(1), cube.get())), cube, cubeLight.get()));
    scene.addLight(move(cubeLight));

    // Teapot
    shared_ptr<Material> teapotMatte = make_shared<Matte>(RGBSpectrum(0.75));
    MeshInstance* teapotMesh = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("teapot"), t7));
    teapotMesh->addMaterial(teapotMatte);
}

void SceneBuilder::createSphereFlakeScene(Scene &scene, int width, int height) {
    Vector origin(-1.5, 3.5, -3);
    Vector destination(0, 0.5, 0);
    Vector lookup(0, 1, 0);
    double fov = 60;

    scene.setCamera(new PerspectiveCamera(width, height, origin, destination - origin, lookup, fov));

    shared_ptr<Material> whiteMatte = make_shared<Matte>(RGBSpectrum::fromRGB(255, 230, 0));

    // plane
    Transformation planeT;
    planeT.scale(100, 100, 100);
    shared_ptr<MeshData> planeMeshData = WaveFrontParser::importMesh("plane");
    MeshInstance* planeMesh = scene.addMesh(make_unique<MeshInstance>(planeMeshData, planeT));
    planeMesh->addMaterial(whiteMatte);

    Transformation t;
    t.translate(Vector(0, 2.0/3, 0));

    shared_ptr<Material> pinkMatte = make_shared<Matte>(RGBSpectrum::fromRGB(255, 0, 255));

    vector<shared_ptr<Material>> flakeMats;
    flakeMats.push_back(make_shared<Matte>(RGBSpectrum::fromRGB(153, 51, 255))); // Purple
    flakeMats.push_back(make_shared<Matte>(RGBSpectrum::fromRGB(102, 0, 204))); // Dark Purple
    flakeMats.push_back(pinkMatte); // Pink
    flakeMats.push_back(make_shared<Matte>(RGBSpectrum::fromRGB(153, 51, 255))); // Purple
    flakeMats.push_back(make_shared<Matte>(RGBSpectrum::fromRGB(102, 0, 204))); // Dark Purple
    flakeMats.push_back(pinkMatte); // Pink

    scene.addPrimitive(make_shared<Primitive>(pinkMatte, make_shared<Sphere>(t)));
    createSphereFlake(scene, flakeMats, t, 4);

    scene.addLight(make_unique<PointLight>(Point(-5, 10, -8), RGBSpectrum(1), 300));
    scene.addLight(make_unique<PointLight>(Point(-5, 10, 8), RGBSpectrum(1), 300));
    scene.addLight(make_unique<PointLight>(Point(10, 10, 0), RGBSpectrum(1), 300));
    scene.addLight(make_unique<GlobalLight>(RGBSpectrum(1), 0.005));

}

void SceneBuilder::createSphereFlake(Scene &scene, const vector<shared_ptr<Material>> &materials,
        const Transformation &t, unsigned int depth) {
    if(depth == 0) return;

    const vector<shared_ptr<Material>> otherMats(materials.begin() + 1, materials.end());

    for(unsigned i = 0; i < 9; i++) {
        Transformation t2;
        double theta;
        double phi;
        if(i < 6) {
            theta = 360.0 / 6 * i;
            phi = 0;
        } else {
            theta = 360.0 / 3 * (i - 6) - 360.0 / 12;
            phi = -50;
        }

        t2.rotateX(90);
        t2.scale(1.0/3, 1.0/3, 1.0/3);
        t2.translate(Vector(0, 0, 4.0/3));
        t2.rotateX(phi);
        t2.rotateY(theta);

        t2 = t * t2;

        scene.addPrimitive(make_shared<Primitive>(materials.front(), make_shared<Sphere>(t2)));

        createSphereFlake(scene, otherMats, t2, depth - 1);
    }
}

void SceneBuilder::createSponzaScene(Scene &scene, int width, int height) {
    Vector origin(0.7, 0.45, 0.05);
    Vector destination(0, 0.25, 0);
    Vector lookup(0, 1, 0);
    double fov = 60;

    scene.setCamera(new PerspectiveCamera(width, height, origin, destination - origin, lookup, fov));

    Transformation t;

    MeshInstance* archMesh          = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/arch"           ), t));
    MeshInstance* bricksMesh        = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/bricks"         ), t));
    MeshInstance* ceilingMesh       = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/ceiling"        ), t));
    MeshInstance* columnAMesh       = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/column_a"       ), t));
    MeshInstance* columnBMesh       = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/column_b"       ), t));
    MeshInstance* columnCMesh       = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/column_c"       ), t));
    MeshInstance* curtainBlueMesh   = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/curtain_blue"   ), t));
    MeshInstance* curtainGreenMesh  = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/curtain_green"  ), t));
    MeshInstance* curtainRedMesh    = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/curtain_red"    ), t));
    MeshInstance* decorationsMesh   = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/decorations"    ), t));
    MeshInstance* detailsMesh       = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/details"        ), t));
    MeshInstance* fabricBlueMesh    = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/fabric_blue"    ), t));
    MeshInstance* fabricGreenMesh   = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/fabric_green"   ), t));
    MeshInstance* fabricRedMesh     = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/fabric_red"     ), t));
    MeshInstance* flagpoleMesh      = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/flagpole"       ), t));
    MeshInstance* floorMesh         = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/floor"          ), t));
    MeshInstance* lionheadMesh      = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/lionhead"       ), t));
    MeshInstance* roofMesh          = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/roof"           ), t));
    MeshInstance* vaseMesh          = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/vase"           ), t));
    MeshInstance* vaseHangingMesh   = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/vase_hanging"   ), t));
    MeshInstance* vaseRoundMesh     = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("sponza/vase_round"     ), t));

    shared_ptr<Material> archText           = make_shared<Textured>("sponza/arch.png"           );
    shared_ptr<Material> bricksText         = make_shared<Textured>("sponza/bricks.png"         );
    shared_ptr<Material> ceilingText        = make_shared<Textured>("sponza/ceiling.png"        );
    shared_ptr<Material> columnAText        = make_shared<Textured>("sponza/column_a.png"       );
    shared_ptr<Material> columnBText        = make_shared<Textured>("sponza/column_b.png"       );
    shared_ptr<Material> columnCText        = make_shared<Textured>("sponza/column_c.png"       );
    shared_ptr<Material> curtainBlueText    = make_shared<Textured>("sponza/curtain_blue.png"   );
    shared_ptr<Material> curtainGreenText   = make_shared<Textured>("sponza/curtain_green.png"  );
    shared_ptr<Material> curtainRedText     = make_shared<Textured>("sponza/curtain_red.png"    );
    shared_ptr<Material> decorationsText    = make_shared<Textured>("sponza/decorations.png"    );
    shared_ptr<Material> detailsText        = make_shared<Textured>("sponza/details.png"        );
    shared_ptr<Material> fabricBlueText     = make_shared<Textured>("sponza/fabric_blue.png"    );
    shared_ptr<Material> fabricGreenText    = make_shared<Textured>("sponza/fabric_green.png"   );
    shared_ptr<Material> fabricRedText      = make_shared<Textured>("sponza/fabric_red.png"     );
    shared_ptr<Material> flagpoleText       = make_shared<Textured>("sponza/flagpole.png"       );
    shared_ptr<Material> floorText          = make_shared<Textured>("sponza/floor.png"          );
    shared_ptr<Material> lionheadText       = make_shared<Textured>("sponza/lionhead.png"       );
    shared_ptr<Material> roofText           = make_shared<Textured>("sponza/roof.png"           );
    shared_ptr<Material> vaseText           = make_shared<Textured>("sponza/vase.png"           );
    shared_ptr<Material> vaseHangingText    = make_shared<Textured>("sponza/vase_hanging.png"   );
    shared_ptr<Material> vaseRoundText      = make_shared<Textured>("sponza/vase_round.png"     );

    archMesh        ->addMaterial(archText        );
    bricksMesh      ->addMaterial(bricksText      );
    ceilingMesh     ->addMaterial(ceilingText     );
    columnAMesh     ->addMaterial(columnAText     );
    columnBMesh     ->addMaterial(columnBText     );
    columnCMesh     ->addMaterial(columnCText     );
    curtainBlueMesh ->addMaterial(curtainBlueText );
    curtainGreenMesh->addMaterial(curtainGreenText);
    curtainRedMesh  ->addMaterial(curtainRedText  );
    decorationsMesh ->addMaterial(decorationsText );
    detailsMesh     ->addMaterial(detailsText     );
    fabricBlueMesh  ->addMaterial(fabricBlueText  );
    fabricGreenMesh ->addMaterial(fabricGreenText );
    fabricRedMesh   ->addMaterial(fabricRedText   );
    flagpoleMesh    ->addMaterial(flagpoleText    );
    floorMesh       ->addMaterial(floorText       );
    lionheadMesh    ->addMaterial(lionheadText    );
    roofMesh        ->addMaterial(roofText        );
    vaseMesh        ->addMaterial(vaseText        );
    vaseHangingMesh ->addMaterial(vaseHangingText );
    vaseRoundMesh   ->addMaterial(vaseRoundText   );

    // Lighting
    scene.addLight(make_unique<GlobalLight>(RGBSpectrum(1), 0.05));
    scene.addLight(make_unique<PointLight>(Point(10, 100, 10), RGBSpectrum(1), 30000));
}

void SceneBuilder::createMuseumScene(Scene &scene, int width, int height) {
    Vector origin(0.4, 0.06, 0.05);
    Vector destination(-0.2, 0.1, -0.05);
    Vector lookup(0, 1, 0);
    double fov = 60;

    scene.setCamera(new PerspectiveCamera(width, height, origin, destination - origin, lookup, fov));

    Transformation t;

    MeshInstance* boneMesh              = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("museum/bone"            ), t));
    MeshInstance* brushedmetalsMesh     = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("museum/brushedmetals"   ), t));
    MeshInstance* floorMesh             = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("museum/floor"           ), t));
    MeshInstance* glassMesh             = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("museum/glass"           ), t));
    MeshInstance* glasstranslucentMesh  = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("museum/glasstranslucent"), t));
    MeshInstance* paintedMesh           = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("museum/painted"         ), t));
    MeshInstance* stoneMesh             = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("museum/stone"           ), t));
    MeshInstance* stoneunfinishedMesh   = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("museum/stoneunfinished" ), t));
    MeshInstance* woodpaintedMesh       = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("museum/woodpainted"     ), t));
    MeshInstance* woodvarnishedMesh     = scene.addMesh(make_unique<MeshInstance>(WaveFrontParser::importMesh("museum/woodvarnished"   ), t));

    shared_ptr<Material> whiteMatte = make_shared<Matte>(RGBSpectrum(1));

    boneMesh            ->addMaterial(whiteMatte);
    brushedmetalsMesh   ->addMaterial(whiteMatte);
    floorMesh           ->addMaterial(whiteMatte);
    glassMesh           ->addMaterial(whiteMatte);
    glasstranslucentMesh->addMaterial(whiteMatte);
    paintedMesh         ->addMaterial(whiteMatte);
    stoneMesh           ->addMaterial(whiteMatte);
    stoneunfinishedMesh ->addMaterial(whiteMatte);
    woodpaintedMesh     ->addMaterial(whiteMatte);
    woodvarnishedMesh   ->addMaterial(whiteMatte);

    // Lighting
    unique_ptr<Light> windowLight = make_unique<AreaLight>(RGBSpectrum(1), 8, nullptr, glasstranslucentMesh);
    Light* windowL = scene.addLight(move(windowLight));
    glasstranslucentMesh->addLight(windowL);
}
