#ifndef RAY_TRACER_SCENEBUILDER_H
#define RAY_TRACER_SCENEBUILDER_H


#include <string>
#include "Scene.h"

class SceneBuilder {

public:
    static void createScene(Scene &scene, int width, int height, const std::string &sceneName);

private:
    static void createDiffuseTest1Scene(Scene &scene, int width, int height);
    static void createVeachScene(Scene &scene, int width, int height);
    static void createAreaLight1Scene(Scene &scene, int width, int height);
    static void createAreaLight2Scene(Scene &scene, int width, int height);
    static void createSphereFlakeScene(Scene &scene, int width, int height);
    static void createSponzaScene(Scene &scene, int width, int height);
    static void createMuseumScene(Scene &scene, int width, int height);

    static void createSphereFlake(Scene &scene, const std::vector<std::shared_ptr<Material>> &materials,
            const Transformation &t, unsigned depth);
};


#endif //RAY_TRACER_SCENEBUILDER_H
