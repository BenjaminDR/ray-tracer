#ifndef RAY_TRACER_PRIMITIVE_H
#define RAY_TRACER_PRIMITIVE_H


#include <Material.h>

#include <utility>
#include <Shadeable.h>
#include "Shape.h"
#include "Light.h"
#include "Intersectable.h"

class Primitive : public Intersectable, public Shadeable {

public:
    Primitive(std::shared_ptr<Material> m, std::shared_ptr<Shape> s, const Light *l = nullptr) :
        material(std::move(m)), shape(std::move(s)), light(l) {}
    Primitive(const Primitive&) = delete;
    bool intersect(const Ray &ray, HitObject &hitObject, double maxDist) const override;
    [[nodiscard]] const AABoundingBox& getBoundingBox() const;

    [[nodiscard]] const Light* getLight() const override;
    [[nodiscard]] const Material* getMaterial() const override;

private:
    double distanceTo(const Ray &ray) const override;

    bool encloses(const Point &p) const override;

    double recursiveSurfaceArea() const override;

public:
    const std::shared_ptr<Shape> shape;

private:
    const std::shared_ptr<Material> material;
//    const std::shared_ptr<Shape> shape;
    const Light *light;

};


#endif //RAY_TRACER_PRIMITIVE_H
