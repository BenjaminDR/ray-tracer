#ifndef RAY_TRACER_WAVEFRONTPARSER_H
#define RAY_TRACER_WAVEFRONTPARSER_H


#include "mesh/MeshData.h"

class WaveFrontParser {

public:
    static std::shared_ptr<MeshData> importMesh(const std::string &objectName);

};


#endif //RAY_TRACER_WAVEFRONTPARSER_H
