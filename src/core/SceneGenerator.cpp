#include <geometry/Sphere.h>
#include <material/Matte.h>
#include <light/PointLight.h>
#include <light/GlobalLight.h>
#include "SceneGenerator.h"
#include <random>
#include <core/WaveFrontParser.h>

void SceneGenerator::generateCamera(Scene &scene, int width, int height) {
    const AABoundingBox& topAABB = scene.getTopAABB();

    Transformation t;
    t.scale(5 * topAABB.getWidthForAxis(0), 5 * topAABB.getWidthForAxis(1), 5 * topAABB.getWidthForAxis(2));
    t.translate(topAABB.getCentroid());
    Sphere s(t);
    Normal n;

    Vector origin = s.getRandomSurfacePoint(Point(0, 0, 100), n);
    Vector destination(topAABB.getCentroid());
    Vector lookup(0, 1, 0);
    double fov = 20;

    scene.setCamera(new PerspectiveCamera(width, height, origin, destination - origin, lookup, fov));
}

void SceneGenerator::generateShapeScene(Scene &scene, Generation_Shape shape, unsigned int count, Generation_Spread spread,
                                    double noiseRatio) {
    using namespace std;
    std::default_random_engine re(time(nullptr)%1000);
    std::uniform_real_distribution<double> uni(0, 1);

    shared_ptr<Material> yellowMatte = make_shared<Matte>(RGBSpectrum::fromRGB(255, 230, 0));

    int n = std::ceil(std::cbrt(count));
    double scale = 1.0 / (3 * (n+1));

    Transformation t, t2;
    t.scale(scale, scale, scale);

    if(spread == Generation_Spread::Regular) {
        t.translate(Vector(scale, scale, scale));

        for(unsigned x = 0; x < n; x++) {
            for(unsigned y = 0; y < n; y++) {
                for(unsigned z = 0; z < n; z++) {
                    if(x * n * n + y * n + z + 1 > count) break;
                    t2 = t;
                    if(noiseRatio != 0) {
                        double scaleNoise = pow(uni(re) + 0.5, noiseRatio);
                        t2.scale(scaleNoise, scaleNoise, scaleNoise);
                    }
                    t2.translate(Vector(3 * x * scale, 3 * y * scale, 3 * z * scale));
//                t.translate(-Vector(scale, scale, scale));
//                t2.rotateX(25);
//                t2.rotateY(25);
//                t2.rotateZ(25);
//                t.translate(Vector(scale, scale, scale));
                    scene.addPrimitive(make_shared<Primitive>(yellowMatte, createGenerationShape(shape, t2)));
                }
            }
        }
        scene.shufflePrimitives();

    } else if(spread == Generation_Spread::UniformlyDistributed) {
        for(unsigned i = 0; i < count; i++) {
            t2 = t;
            if(noiseRatio != 0) {
                double scaleNoise = pow(uni(re) + 0.5, noiseRatio);
                t2.scale(scaleNoise, scaleNoise, scaleNoise);
            }
            t2.translate(Vector(uni(re), uni(re), uni(re)));
            scene.addPrimitive(make_shared<Primitive>(yellowMatte, createGenerationShape(shape, t2)));
        }
    } else if(spread == Generation_Spread::NormallyDistributed) {
        std::normal_distribution<double> dist(0.5, 0.2);
        t.scale(0.5, 0.5, 0.5);

        for(unsigned i = 0; i < count; i++) {
            t2 = t;
            if(noiseRatio != 0) {
                double scaleNoise = pow(uni(re) + 0.5, noiseRatio);
                t2.scale(scaleNoise, scaleNoise, scaleNoise);
            }
            t2.translate(Vector(dist(re), dist(re), dist(re)));
            scene.addPrimitive(make_shared<Primitive>(yellowMatte, createGenerationShape(shape, t2)));
        }
    } else if(spread == Generation_Spread::ExponentiallyDistributed) {
        std::exponential_distribution<double> dist(1);
        t.scale(0.5, 0.5, 0.5);

        for(unsigned i = 0; i < count; i++) {
            t2 = t;
            if(noiseRatio != 0) {
                double scaleNoise = pow(uni(re) + 0.5, noiseRatio);
                t2.scale(scaleNoise, scaleNoise, scaleNoise);
            }
            t2.translate(Vector(dist(re), dist(re), dist(re)));
            scene.addPrimitive(make_shared<Primitive>(yellowMatte, createGenerationShape(shape, t2)));
        }
    } else {
        std::cerr << "Invalid generation spread!\n";
        exit(1);
    }

    scene.addLight(make_unique<PointLight>(Point(5, 12, 10), RGBSpectrum(1), 300));
    scene.addLight(make_unique<PointLight>(Point(-5, -12, 10), RGBSpectrum(1), 300));
    scene.addLight(make_unique<GlobalLight>(RGBSpectrum(1), 0.001));
}

std::shared_ptr<Shape> SceneGenerator::createGenerationShape(Generation_Shape shape, const Transformation &t) {
    if(shape == Generation_Shape::Sphere) return std::make_shared<Sphere>(t);
    else {
        std::cerr << "Invalid generation shape!\n";
        exit(1);
    }
}

void SceneGenerator::generateMeshScene(Scene &scene, Generation_Mesh mesh, unsigned int count, Generation_Spread spread,
                                       double noiseRatio) {
    using namespace std;
    std::default_random_engine re(time(nullptr)%1000);
    std::uniform_real_distribution<double> uni(0, 1);

    shared_ptr<Material> yellowMatte = make_shared<Matte>(RGBSpectrum::fromRGB(255, 230, 0));
    shared_ptr<MeshData> meshData = createGenerationMeshData(mesh);

    int n = std::ceil(std::cbrt(count));
    double scale = 1.0 / (3 * (n+1));

    Transformation t, t2;
    t.scale(scale, scale, scale);

    if(spread == Generation_Spread::Regular) {
        t.translate(Vector(scale, scale, scale));

        for(unsigned x = 0; x < n; x++) {
            for(unsigned y = 0; y < n; y++) {
                for(unsigned z = 0; z < n; z++) {
                    if(x * n * n + y * n + z + 1 > count) break;
                    t2 = t;
                    if(noiseRatio != 0) {
                        double scaleNoise = pow(uni(re) + 0.5, noiseRatio);
                        t2.scale(scaleNoise, scaleNoise, scaleNoise);
                    }
                    t2.translate(Vector(3 * x * scale, 3 * y * scale, 3 * z * scale));
                    MeshInstance* meshInstance = scene.addMesh(make_unique<MeshInstance>(meshData, t2));
                    meshInstance->addMaterial(yellowMatte);
                }
            }
        }
    } else if(spread == Generation_Spread::UniformlyDistributed) {
        for(unsigned i = 0; i < count; i++) {
            t2 = t;
            if(noiseRatio != 0) {
                double scaleNoise = pow(uni(re) + 0.5, noiseRatio);
                t2.scale(scaleNoise, scaleNoise, scaleNoise);
            }
            t2.translate(Vector(uni(re), uni(re), uni(re)));
            MeshInstance* meshInstance = scene.addMesh(make_unique<MeshInstance>(meshData, t2));
            meshInstance->addMaterial(yellowMatte);
        }
    } else if(spread == Generation_Spread::NormallyDistributed) {
        std::normal_distribution<double> dist(0.5, 0.2);
        t.scale(0.5, 0.5, 0.5);

        for(unsigned i = 0; i < count; i++) {
            t2 = t;
            if(noiseRatio != 0) {
                double scaleNoise = pow(uni(re) + 0.5, noiseRatio);
                t2.scale(scaleNoise, scaleNoise, scaleNoise);
            }
            t2.translate(Vector(dist(re), dist(re), dist(re)));
            MeshInstance* meshInstance = scene.addMesh(make_unique<MeshInstance>(meshData, t2));
            meshInstance->addMaterial(yellowMatte);
        }
    } else if(spread == Generation_Spread::ExponentiallyDistributed) {
        std::exponential_distribution<double> dist(1);
        t.scale(0.5, 0.5, 0.5);

        for(unsigned i = 0; i < count; i++) {
            t2 = t;
            if(noiseRatio != 0) {
                double scaleNoise = pow(uni(re) + 0.5, noiseRatio);
                t2.scale(scaleNoise, scaleNoise, scaleNoise);
            }
            t2.translate(Vector(dist(re), dist(re), dist(re)));
            MeshInstance* meshInstance = scene.addMesh(make_unique<MeshInstance>(meshData, t2));
            meshInstance->addMaterial(yellowMatte);
        }
    } else {
        std::cerr << "Invalid generation spread!\n";
        exit(1);
    }

    scene.addLight(make_unique<PointLight>(Point(5, 12, 10), RGBSpectrum(1), 300));
    scene.addLight(make_unique<PointLight>(Point(-5, -12, 10), RGBSpectrum(1), 300));
    scene.addLight(make_unique<GlobalLight>(RGBSpectrum(1), 0.001));
}

std::shared_ptr<MeshData> SceneGenerator::createGenerationMeshData(Generation_Mesh mesh) {
    if(mesh == Generation_Mesh::Venus) return WaveFrontParser::importMesh("venus");
    else if(mesh == Generation_Mesh::TRex) return WaveFrontParser::importMesh("trex");
    else {
        std::cerr << "Invalid generation mesh!\n";
        exit(1);
    }
}

