#include "WaveFrontParser.h"
#include <fstream>

using namespace std;

std::shared_ptr<MeshData> WaveFrontParser::importMesh(const string &objectName) {
    vector<Point> vertices;
    vector<Point2d> textureCoords;
    vector<Normal> vertexNormals;
    vector<int> triangleInformation;

    ifstream objectFile("models/" + objectName + ".obj");
    string line;
    string keyword;

    while(getline(objectFile, line)) {
        istringstream lineStream(line);

        lineStream >> keyword;
        if(keyword == "v") {
            double x, y, z;
            lineStream >> x >> y >> z;
            vertices.emplace_back(x, y, z);
        } else if(keyword == "vt") {
            double u, v;
            lineStream >> u >> v;
            textureCoords.emplace_back(u, v);
        } else if(keyword == "vn") {
            double x, y, z;
            lineStream >> x >> y >> z;
            vertexNormals.emplace_back(x, y, z);
        } else if (keyword == "f") {
            string triangleInfo;
            int v, vt, vn;
            for(int i = 0; i < 3; i++) {
                lineStream >> triangleInfo;
                replace(triangleInfo.begin(), triangleInfo.end(), '/', ' ');
                istringstream triangleStream(triangleInfo);
                triangleStream >> v >> vt >> vn;
                triangleInformation.push_back(v - 1);
                triangleInformation.push_back(vt - 1);
                triangleInformation.push_back(vn - 1);
            }
        }

    }
    return std::make_shared<MeshData>(vertices, textureCoords, vertexNormals, triangleInformation);
}
