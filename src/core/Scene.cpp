#include <math/Transformation.h>
#include <Camera.h>
#include <thread>
#include <film/Sampler.h>
#include "Scene.h"
#include "Stats.h"


using namespace std;

Scene::~Scene() {
    // Free camera
    delete camera;
}


void Scene::render(const FrameBuffer &buffer, unsigned start, unsigned end, unsigned spp, unsigned shadowRays) {
    assert(camera);
    unsigned sqrtSpp = floor(sqrt(spp));
    assert(sqrtSpp * sqrtSpp == spp);

    for(unsigned x = start; x < end; x++) {
        RGBSpectrum color;
        std::vector<Sample> samples = Sampler::generateSamples(x % buffer.xRes, (unsigned) buffer.yRes - x / buffer.xRes,
            sqrtSpp, SamplingMethod::JITTERED);
        for(const Sample& sample : samples) {
            const Ray ray = camera->generateRay(sample);
            HitObject closestHit;
            bool intersection = getClosestIntersection(ray, closestHit);
            if(intersection) color += shader.shade(*this, closestHit, shadowRays);
        }
        buffer.getPixel(x).add(color / spp, spp);
    }
}

void Scene::render(const FrameBuffer &buffer, unsigned spp, unsigned shadowRays, bool singleThreaded=false) {
    assert(built);

    if(singleThreaded) {
        render(buffer, 0, buffer.xRes * buffer.yRes, spp, shadowRays);
        return;
    }

    const int pixelCount = buffer.xRes * buffer.yRes;
//    const int maxThreads = min(pixelCount, (int) thread::hardware_concurrency());
    const int maxThreads = 16;
    const int pixelsPerThread = pixelCount / maxThreads;
    vector<thread> threads;
    threads.reserve(maxThreads);

    for(int x = 0; x < maxThreads; x++) {
        threads.emplace_back([this, &buffer, x, pixelsPerThread, pixelCount, spp, shadowRays]() {
            this->render(buffer, x * pixelsPerThread, min((x+1) * pixelsPerThread, pixelCount ), spp, shadowRays);
            STAT_END_THREADED(BBintersects, BBintersects_l)
            STAT_END_THREADED(Shapeintersects, Shapeintersects_l)
        });
    }

    for(auto& t : threads) {
        t.join();
    }

}

std::vector<Light *> Scene::getLights() const {
    std::vector<Light*> light_ptrs;
    light_ptrs.reserve(lights.size());

    for(const auto& l : lights) light_ptrs.push_back(l.get());

    return light_ptrs;
}

bool Scene::getClosestIntersection(const Ray &ray, HitObject &closestHit, const double maxDistance) {
    bool intersection = false;

    if(bvh.isBuilt()) intersection = bvh.intersect(ray, closestHit, maxDistance);
    else {
        HitObject lastHit;
        for(auto const &primitive: primitives) {
            if(primitive->intersect(ray, lastHit, maxDistance)) {
                intersection = true;
                if(lastHit.distance < closestHit.distance) {
                    closestHit = lastHit;
                    closestHit.shadeable = primitive.get();
                }
            }
        }
    }

    return intersection;
}

bool Scene::isIntersection(const Ray &ray, double maxDistance) const {
    if(maxDistance == 0) return false;

    HitObject lastHit;
    if(bvh.isBuilt()) return bvh.intersect(Ray(ray.origin + ray.direction * EPSILON, ray.direction), lastHit, maxDistance - 2 * EPSILON);


    for(auto const &primitive: primitives) {
        if(primitive->intersect(ray, lastHit, maxDistance)) {
            if(lastHit.distance > EPSILON) return true;
        }
    }
    return false;
}

Light* Scene::addLight(std::unique_ptr<Light> light) {
    lights.push_back(std::move(light));
    return lights.back().get();
}

void Scene::addPrimitive(const std::shared_ptr<Primitive>& primitive) {
    primitives.push_back(primitive);
}

void Scene::setCamera(Camera *cam) {
    camera = cam;
}

void Scene::addPrimitives(const std::vector<shared_ptr<Primitive>> &prims) {
    primitives.insert(primitives.end(), prims.begin(), prims.end());
}

void Scene::buildBVH(BVH_Partition_Strategy strategy, BVH_Axis_Order order) {
    assert(!built);

    if(primitives.size() + meshes.size() == 0) return;

    interInfo.reserve(primitives.size() + meshes.size());
    for(const auto& mesh : meshes) {
        IntersectableInfo i;
        i.intersectable = mesh->buildBVH(strategy, order);
        i.boundingBox = mesh->getBoundingBox();
        i.centroid = i.boundingBox.getCentroid();
        interInfo.push_back(i);
    }
    for(const std::shared_ptr<Primitive>& prim : primitives) {
        IntersectableInfo i;
        i.intersectable = prim;
        i.boundingBox = prim->getBoundingBox();
        i.centroid = i.boundingBox.getCentroid();
        interInfo.push_back(i);
    }

    auto infoPointers = new IntersectableInfo*[interInfo.size()];
    for(int i = 0; i < interInfo.size(); i++) {
        infoPointers[i] = &interInfo[i];
    }
    bvh.build(infoPointers, interInfo.size(), 0, strategy, order);

    built = true;
    delete[] infoPointers;
    interInfo.clear();
}

MeshInstance* Scene::addMesh(std::unique_ptr<MeshInstance> mesh) {
    meshes.push_back(std::move(mesh));
    return meshes.back().get();
}

void Scene::renderAccelerationStructure(const FrameBuffer &buffer) const {
    assert(camera);
    assert(bvh.isBuilt());
    for(int x = 0; x < buffer.xRes * buffer.yRes; x++) {
        const Ray ray = camera->generateRay(Sample(x % buffer.xRes + 0.5, (int) (buffer.yRes - x / buffer.xRes) + 0.5));
        int boxesCount = bvh.interSectionCount(ray, 0, 0, 9999);
        buffer.getPixel(x).add(RGBSpectrum(0.0026415, 0.027734375, 0.02734375) / 10 * boxesCount, 1);
    }
}

void Scene::buildTriangleSoup() {
    assert(! built);
    for(const auto& mesh : meshes) {
        addPrimitives(mesh->createFlatPrimitives());
    }
    meshes.clear();
    built = true;
    shader = Shader();
}

double Scene::evaluateBVH() const {
    return bvh.evaluateCost();
}

void Scene::removeBVH() {
    bvh.clear();
    built = false;
}

const AABoundingBox &Scene::getTopAABB() const {
    return bvh.getTopAABB();
}

void Scene::shufflePrimitives() {
    auto rd = std::random_device {};
    auto rng = std::default_random_engine { rd() };
    std::shuffle(std::begin(primitives), std::end(primitives), rng);
}

