#include "Primitive.h"
#include "Stats.h"

bool Primitive::intersect(const Ray &ray, HitObject &hitObject, double maxDist) const {
    STAT_OP(Shapeintersects_l++)
    HitObject hitCandidate;
    bool intersection = shape->intersect(ray, hitCandidate);
    if(intersection){
        if(hitCandidate.distance > maxDist) return false;
        hitObject = hitCandidate;
        hitObject.shadeable = this;
    }
    return intersection;
}

const AABoundingBox &Primitive::getBoundingBox() const {
    return shape->getBoundingBox();
}

const Light *Primitive::getLight() const {
    return light;
}

const Material *Primitive::getMaterial() const {
    return material.get();
}

double Primitive::distanceTo(const Ray &ray) const {
    return 0;
}

bool Primitive::encloses(const Point &p) const {
    return false;
}

double Primitive::recursiveSurfaceArea() const {
    return shape->surfaceArea();
}
