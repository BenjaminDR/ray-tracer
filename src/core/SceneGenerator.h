#ifndef RAY_TRACER_SCENEGENERATOR_H
#define RAY_TRACER_SCENEGENERATOR_H


#include "Scene.h"

enum class Generation_Shape {
    Sphere,
};

enum class Generation_Mesh {
    Venus,
    TRex,
};

enum class Generation_Spread {
    Regular,
    UniformlyDistributed,
    NormallyDistributed,
    ExponentiallyDistributed,
};

class SceneGenerator {
public:
    static void generateShapeScene(Scene &scene, Generation_Shape shape, unsigned count, Generation_Spread spread,
            double noiseRatio);
    static void generateMeshScene(Scene &scene, Generation_Mesh mesh, unsigned count, Generation_Spread spread,
                                   double noiseRatio);
    static void generateCamera(Scene &scene, int width, int height) ;

private:
    static std::shared_ptr<Shape> createGenerationShape(Generation_Shape shape, const Transformation &t);
    static std::shared_ptr<MeshData> createGenerationMeshData(Generation_Mesh mesh);
};


#endif //RAY_TRACER_SCENEGENERATOR_H
