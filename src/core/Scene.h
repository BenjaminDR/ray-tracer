#ifndef RAY_TRACER_SCENE_H
#define RAY_TRACER_SCENE_H

#include "camera/PerspectiveCamera.h"
#include "Light.h"
#include "core/Primitive.h"
#include "acceleration/BVH.h"
#include "mesh/MeshInstance.h"
#include <film/FrameBuffer.h>
#include <vector>
#include <shading/Shader.h>

class Scene {

public:
    Scene() = default;
    Scene(const Scene &scene) = delete;
    ~Scene();

    void render(const FrameBuffer &buffer, unsigned spp, unsigned shadowRays, bool singleThreaded);
    void renderAccelerationStructure(const FrameBuffer &buffer) const;

    bool getClosestIntersection(const Ray &ray, HitObject &closestHit, double maxDistance = DINF);
    [[nodiscard]] bool isIntersection(const Ray &ray, double maxDistance = DINF) const;

    [[nodiscard]] std::vector<Light*> getLights() const;

    void addPrimitive(const std::shared_ptr<Primitive>& primitive);
    void addPrimitives(const std::vector<std::shared_ptr<Primitive>> &prims);
    void shufflePrimitives();
    Light* addLight(std::unique_ptr<Light> light);
    void setCamera(Camera *cam);
    void buildBVH(BVH_Partition_Strategy strategy, BVH_Axis_Order order);
    void removeBVH();
    [[nodiscard]] double evaluateBVH() const;
    MeshInstance* addMesh(std::unique_ptr<MeshInstance> mesh);
    [[nodiscard]] const AABoundingBox& getTopAABB() const;

    void buildTriangleSoup();

private:
    std::vector<std::shared_ptr<Primitive>> primitives;
    std::vector<std::unique_ptr<MeshInstance>> meshes;
    std::vector<std::unique_ptr<Light>> lights;
    std::vector<IntersectableInfo> interInfo;
    Camera* camera = nullptr;
    BVH bvh;
    Shader shader;

    bool built = false;

    void render(const FrameBuffer &buffer, unsigned start, unsigned end, unsigned spp, unsigned shadowRays);

};


#endif //RAY_TRACER_SCENE_H
