#include <iostream>
#include <film/FrameBuffer.h>
#include <core/Scene.h>
#include <getopt.h>
#include <core/SceneBuilder.h>
#include <chrono>
#include "Stats.h"

STAT_INIT_THREADED(BBintersects, BBintersects_l)
STAT_INIT_THREADED(Shapeintersects, Shapeintersects_l)

using namespace std;
using namespace Eigen;

void printHelp();
string prettyPrintElapsed(unsigned long long elapsed);

int main(int argc, char* argv[]) {

    double sensitivity = 1;
    double gamma = 2.2;
    bool verbose = false;
    bool singleThreaded = false;
    bool save = true;

    int width = 640;
    int height = 640;
    unsigned spp = 1;
    unsigned shadowRays = 1;
    string outputFileName = "output";
    string sceneName = "Veach";

    const option long_opts[] = {
            {"width", required_argument, nullptr, 'x'},
            {"height", required_argument, nullptr, 'y'},
            {"out", required_argument, nullptr, 'o'},
            {"scene", required_argument, nullptr, 's'},
            {"noSave", no_argument, nullptr, 'n'},
            {"singleThreaded", no_argument, nullptr, 't'},
            {"verbose", no_argument, nullptr, 'v'},
            {"help", no_argument, nullptr, 'h'},
            {"spp", required_argument, nullptr, 'p'},
            {"shadowRays", required_argument, nullptr, 'r'},
            {nullptr, no_argument, nullptr, 0}
    };

    // Parse program short options
    for(;;) {
        switch(getopt_long(argc, argv, "ho:s:tv", long_opts, nullptr)) {
            case 'x':
                width = stoi(optarg);
                continue;
            case 'y':
                height = stoi(optarg);
                continue;
            case 'o':
                outputFileName = optarg;
                continue;
            case 'n':
                save = false;
                continue;
            case 't':
                singleThreaded = true;
                continue;
            case 's':
                sceneName = optarg;
                continue;
            case 'v':
                verbose = true;
                continue;
            case 'p':
                spp = stoi(optarg);
                continue;
            case 'r':
                shadowRays = stoi(optarg);
                continue;
            case 'h':
            default :
                printHelp();
                return -1;
            case -1:
                break;
        }
        break;
    }

    // Let Eigen know that we're using multiple threads
    if(! singleThreaded) Eigen::initParallel();

    if(verbose) cout << "Making FrameBuffer...\n";
    FrameBuffer buffer(width, height);

    cout << "Creating Scene...\n";
    Scene scene;
    auto start = std::chrono::steady_clock::now();
    SceneBuilder::createScene(scene, width, height, sceneName);
//    scene.buildTriangleSoup();
//    SceneGenerator::generateShapeScene(scene, Generation_Shape::Sphere, 30000, Generation_Spread::Regular, 0);
//    SceneGenerator::generateMeshScene(scene, Generation_Mesh::Venus, 1000, Generation_Spread::UniformlyDistributed, 1);
    auto elapsed = (std::chrono::steady_clock::now() - start).count();
    cout << "Scene created in: " << prettyPrintElapsed(elapsed) << endl << endl;

    cout << "Building Acceleration Structures...\n";
    start = std::chrono::steady_clock::now();
    scene.buildBVH(BVH_Partition_Strategy::OBJECT_MEDIAN_SPLIT, BVH_Axis_Order::DOMINANT);
    elapsed = (std::chrono::steady_clock::now() - start).count();
    cout << "Acceleration Structures built in: " << prettyPrintElapsed(elapsed) << endl << endl;

    cout << "Evaluating Acceleration Structures...\n";
    start = std::chrono::steady_clock::now();
    double cost  = scene.evaluateBVH();
    elapsed = (std::chrono::steady_clock::now() - start).count();
    cout << "Acceleration Structures cost: " << cost << endl;
    cout << "Acceleration Structures evaluated in: " << prettyPrintElapsed(elapsed) << endl << endl;


    cout << "Rendering Scene...\n";
    start = std::chrono::steady_clock::now();
//    scene.render(buffer, spp, shadowRays, singleThreaded);
    scene.renderAccelerationStructure(buffer);
    elapsed = (std::chrono::steady_clock::now() - start).count();
    cout << "Scene rendered in: " << prettyPrintElapsed(elapsed) << endl << endl;

#ifdef STATS
    // Print stats
    cout << "Printing statistics...\n";
    cout << "AABB intersections: " << BBintersects << endl;
    cout << "Shape intersections: " << Shapeintersects << endl;
    cout << "Total intersections: " << BBintersects + Shapeintersects << endl << endl;
    cout << "Min/Max BVH depth: " << min_bvh_depth << "/" << max_bvh_depth << endl << endl;
#endif

    // Save buffer to ppm
    if(save) {
        if(verbose) cout << "Saving output to: " << outputFileName << ".ppm\n";
        buffer.saveToPPM(outputFileName, gamma, sensitivity);
    }

    return 0;
}

void printHelp() {
    cout << "This is an executable for the ray-tracer application: https://gitlab.com/BenjaminDR/ray-tracer" << endl
        << endl
        << "Usage: " << endl
        << "    ray-tracer [options]" << endl
        << endl
        << "Options: " << endl
        << "    -h, --help              Prints this help information" << endl
        << "    -o, --out=output        Specifies the filename of the output PPM image file." << endl
        << "    -s, --scene             Specifies the scene to be loaded." << endl
        << "    -t, --singleThreaded    Executes this program using only the main thread." << endl
        << "    -v, --verbose           Enables verbose output." << endl
        << "    --width=640             Specifies the output image width." << endl
        << "    --height=640            Specifies the output image height." << endl
        << "    --noSave                Do not save the resulting image." << endl
        << "    --spp=1                 Specifies the samples per pixel used for rendering." << endl
        << "    --shadowRays=1          Specifies the amount of shadowRays cast, zero means no shadows will be rendered." << endl
        << endl;

}

string prettyPrintElapsed(unsigned long long elapsed) {
    string out;
    bool f = false;
    auto h = elapsed / 3600000000000;
    if(h != 0) {
        out += std::to_string(h) + "h ";
        f = true;
    }
    auto m = (elapsed % 3600000000000) / 60000000000;
    if(m != 0 || f) {
        out += std::to_string(m) + "m ";
        f = true;
    }
    auto s = (elapsed % 60000000000) / 1000000000;
    if(s != 0 || f) out += std::to_string(s) + "s ";
    out += std::to_string((elapsed % 1000000000) / 1000000) + "ms";
    return out;
}

