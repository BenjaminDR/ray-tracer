#include "PointLight.h"

#include <utility>


PointLight::PointLight(Point pos, const RGBSpectrum &col, double pow) : Light(col, pow), position(std::move(pos)) {}

RGBSpectrum PointLight::getRadiance(const Point &origin, const Normal &normal, Ray &shadowRay) {
    Vector l = position - origin;
    double distToLight = l.squaredNorm();
    double lDotNormal = normal.dot(l.normalized());
    if(lDotNormal < 0) return RGBSpectrum();
    shadowRay = Ray(origin, l);
    double factor = lDotNormal * M_1_PI / (4 * distToLight);
    return Le() * factor;
}

double PointLight::pdf() const {
    return 1;
}

