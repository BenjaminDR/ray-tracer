#include "AreaLight.h"

RGBSpectrum AreaLight::getRadiance(const Point &origin, const Normal &normal, Ray &shadowRay) {
    Normal pNorm;
    Point position;

    if(shape) position = shape->getRandomSurfacePoint(origin, pNorm);
    else position = mesh->getRandomSurfacePoint(origin, pNorm);

    Vector l = position - origin;
    double distToLight = l.squaredNorm();
    double lDotNormal = normal.dot(l.normalized());
    if(lDotNormal < 0) return RGBSpectrum();
    shadowRay = Ray(origin, l);

    double cosDeltaI = pNorm.dot(-l);

    double factor = lDotNormal * cosDeltaI / distToLight;
    return Le() * factor;
}

RGBSpectrum AreaLight::Le() const {
    if(shape) return color * power * M_1_PI * 1/shape->surfaceArea();
    else return color * power * M_1_PI * 1/mesh->surfaceArea();
}

double AreaLight::pdf() const {
    if(shape) return 2/shape->surfaceArea(); // probability 2 times larger because only hemisphereOfSurface is tested.
    else return 1/mesh->surfaceArea();
}
