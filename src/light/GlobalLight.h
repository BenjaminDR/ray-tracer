#ifndef RAY_TRACER_GLOBALLIGHT_H
#define RAY_TRACER_GLOBALLIGHT_H


#include "Light.h"

class GlobalLight : public Light {

public:
    GlobalLight(const RGBSpectrum &_color, double power);
    RGBSpectrum getRadiance(const Point &origin, const Normal &normal, Ray &shadowRay) override;

    [[nodiscard]] double pdf() const override;

};


#endif //RAY_TRACER_GLOBALLIGHT_H
