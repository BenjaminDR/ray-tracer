#include "GlobalLight.h"

GlobalLight::GlobalLight(const RGBSpectrum &_color, const double _power) : Light(_color, _power) {}

RGBSpectrum GlobalLight::getRadiance(const Point &origin, const Normal &normal, Ray &shadowRay) {
    shadowRay = Ray(Vector(0, 0, 0), Vector(0, 0, 0));
    return Le();
}

double GlobalLight::pdf() const {
    return 1;
}

