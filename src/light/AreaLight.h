#ifndef RAY_TRACER_AREALIGHT_H
#define RAY_TRACER_AREALIGHT_H


#include "Light.h"
#include "Shape.h"
#include "mesh/MeshInstance.h"

class AreaLight : public Light {

public:
    AreaLight(const RGBSpectrum &c, double p, const Shape* s, const MeshInstance* m = nullptr) :
        Light(c, p), shape(s), mesh(m) { assert(shape != nullptr || mesh != nullptr); }

    RGBSpectrum getRadiance(const Point &origin, const Normal &normal, Ray &shadowRay) override;
    [[nodiscard]] RGBSpectrum Le() const override;

    [[nodiscard]] double pdf() const override;

private:
    const Shape *shape;
    const MeshInstance *mesh;
};


#endif //RAY_TRACER_AREALIGHT_H
