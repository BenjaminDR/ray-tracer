#ifndef RAY_TRACER_POINTLIGHT_H
#define RAY_TRACER_POINTLIGHT_H

#include <Light.h>
#include "film/RGBSpectrum.h"
#include "math/Transformation.h"

class PointLight : public Light {

public:
    PointLight(Point pos, const RGBSpectrum &col, double pow);

    RGBSpectrum getRadiance(const Point &origin, const Normal &normal, Ray &shadowRay) override;
    [[nodiscard]] double pdf() const override;

private:
    const Point position;

};


#endif //RAY_TRACER_POINTLIGHT_H
