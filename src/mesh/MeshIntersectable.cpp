#include "MeshIntersectable.h"

bool MeshIntersectable::intersect(const Ray &ray, HitObject &hitObject, double maxDist) const {
    return mesh->intersect(ray, hitObject, maxDist, index);
}

double MeshIntersectable::distanceTo(const Ray &ray) const {
    return 0;
}

bool MeshIntersectable::encloses(const Point &p) const {
    return false;
}

double MeshIntersectable::recursiveSurfaceArea() const {
    return mesh->getSurfaceArea(index);
}
