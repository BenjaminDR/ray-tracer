#ifndef RAY_TRACER_MESHINSTANCE_H
#define RAY_TRACER_MESHINSTANCE_H

#include <memory>
#include <utility>
#include "MeshData.h"
#include "Light.h"
#include <Material.h>
#include <math/Transformation.h>
#include <core/Primitive.h>
#include <acceleration/AABoundingBox.h>

class MeshInstance : public Shadeable {

public:
    MeshInstance(std::shared_ptr<MeshData> d, Transformation trans);

    bool intersect(const Ray &ray, HitObject &hitObject, double maxDist, int triangleIndex) const;

    [[nodiscard]] std::unique_ptr<AABoundingBox> getBoundingBox(int triangleIndex) const;
    [[nodiscard]] AABoundingBox getBoundingBox() const;
    [[nodiscard]] double surfaceArea(int triangleIndex) const;

    std::shared_ptr<Intersectable> buildBVH(BVH_Partition_Strategy strategy, BVH_Axis_Order order);
    [[nodiscard]] std::vector<std::shared_ptr<Primitive>> createFlatPrimitives() const;

    [[nodiscard]] double surfaceArea() const;
    Point getRandomSurfacePoint(const Point &visibleFrom, Normal &normal) const;

    void addLight(Light* l);
    void addMaterial(const std::shared_ptr<Material>& m);

    [[nodiscard]] const Light* getLight() const override;
    [[nodiscard]] const Material* getMaterial() const override;

private:
    const Transformation t;
    std::shared_ptr<Material> material = nullptr;
    std::shared_ptr<MeshData> data;
    Light* light = nullptr;
};


#endif //RAY_TRACER_MESHINSTANCE_H
