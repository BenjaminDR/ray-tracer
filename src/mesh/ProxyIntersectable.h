#ifndef RAY_TRACER_PROXYINTERSECTABLE_H
#define RAY_TRACER_PROXYINTERSECTABLE_H


#include <Intersectable.h>
#include <math/Transformation.h>

#include <utility>

class ProxyIntersectable : public Intersectable {

public:
    ProxyIntersectable(Intersectable* i, Transformation trans, Shadeable* s);
    bool intersect(const Ray &ray, HitObject &hitObject, double maxDist) const override;
    [[nodiscard]] double distanceTo(const Ray &ray) const override;
    [[nodiscard]] bool encloses(const Point &p) const override;
    [[nodiscard]] double recursiveSurfaceArea() const override;

    [[nodiscard]] int interSectionCount(const Ray &ray, int depth, int minDepth, int maxDepth) const override;

private:
    const Transformation t;
    Intersectable* intersectable;
    Shadeable* shadeable;

};


#endif //RAY_TRACER_PROXYINTERSECTABLE_H
