#include <film/Sampler.h>
#include "MeshData.h"
#include "MeshIntersectable.h"



MeshData::MeshData(std::vector<Point> verts, std::vector<Point2d> textCoords, std::vector<Normal> vertNormals,
    std::vector<int> triangs) : vertices(std::move(verts)),
                                textureCoords(std::move(textCoords)),
                                vertexNormals(std::move(vertNormals)),
                                triangles(std::move(triangs)) {

    area = 0;
    for(int i = 0; i < triangles.size(); i += 9) {
        area += getSurfaceArea(i);
//        triangleWeights.push_back(area);
    }
};

BVH * MeshData::buildBVH(BVH_Partition_Strategy strategy, BVH_Axis_Order order) {
    if(bvh != nullptr) return bvh.get();

    bvh = std::make_unique<BVH>();

    info.reserve(triangles.size() / 9);
    for(int i = 0; i < triangles.size(); i+= 9) {
        IntersectableInfo inf;
        // Intersectable
        inf.intersectable = std::make_shared<MeshIntersectable>(this, i);
        // AABB
        Point v1 = vertices[triangles[i]];
        Point v2 = vertices[triangles[i + 3]];
        Point v3 = vertices[triangles[i + 6]];
        inf.boundingBox = AABoundingBox(v1.cwiseMin(v2).cwiseMin(v3), v1.cwiseMax(v2).cwiseMax(v3));
        // Centroid
        inf.centroid = inf.boundingBox.getCentroid();
        info.push_back(inf);
    }

    auto infoPointers = new IntersectableInfo*[info.size()];
    for(int i = 0; i < info.size(); i++) {
        infoPointers[i] = &info[i];
    }

    bvh->build(infoPointers, info.size(), 0, strategy, order);
    info.clear();
    return bvh.get();
}

bool MeshData::intersect(const Ray &ray, HitObject &hitObject, double maxDist, int triangleIndex) const {
    Point v1 = vertices[triangles[triangleIndex]];
    Point v2 = vertices[triangles[triangleIndex + 3]];
    Point v3 = vertices[triangles[triangleIndex + 6]];

    Vector edge1(v2 - v1);
    Vector edge2(v3 - v1);

    Vector h(ray.direction.cross(edge2));

    double det = edge1.dot(h);

    if (det > -EPSILON && det < EPSILON) return false; // Ray is parallel to triangle

    double invDet = 1 / det;

    Vector s(ray.origin - v1);

    double beta = invDet * s.dot(h);
    if (beta < 0 || beta > 1) return false;

    Vector q(s.cross(edge1));

    double gamma = invDet * ray.direction.dot(q);
    if (gamma < 0 || beta + gamma > 1) return false;

    double T = invDet * edge2.dot(q);

    if(T < EPSILON || T > maxDist) return false;

    Normal vn1 = vertexNormals[triangles[triangleIndex + 2]];
    Normal vn2 = vertexNormals[triangles[triangleIndex + 5]];
    Normal vn3 = vertexNormals[triangles[triangleIndex + 8]];
    Point2d vt1 = textureCoords[triangles[triangleIndex + 1]];
    Point2d vt2 = textureCoords[triangles[triangleIndex + 4]];
    Point2d vt3 = textureCoords[triangles[triangleIndex + 7]];

    hitObject.distance = T;
    hitObject.normal = ((1 - beta - gamma) * vn1 + beta * vn2 + gamma * vn3).normalized();
    if(ray.direction.dot(hitObject.normal) > 0) hitObject.normal = -hitObject.normal;
    hitObject.hitPoint = ray.origin + T * ray.direction;
    hitObject.uv = vt1 * (1 - beta - gamma) + vt2 * beta + vt3 * gamma;

    return true;
}

double MeshData::getSurfaceArea(int triangleIndex) const {
    Point v1 = vertices[triangles[triangleIndex]];
    Point v2 = vertices[triangles[triangleIndex + 3]];
    Point v3 = vertices[triangles[triangleIndex + 6]];

    Vector edge1(v2 - v1);
    Vector edge2(v3 - v1);

    return 0.5 * edge1.cross(edge2).norm();
}

Point MeshData::getRandomSurfacePoint(const Point &visibleFrom, Normal &normal, const Transformation &t, double surfaceArea) const {
    // This function is not working because the triangleWeights are (intentionally) not computed.
    // If you want to compute these, you will need to find a way to make it work for multiple dynamic transformations
    assert(false);
    double rand = Sampler::getRandomDouble();
    rand *= surfaceArea;
    for(unsigned i = 0; i < triangleWeights.size(); i++){
        if(triangleWeights[i] >= rand){
            Sample sample = Sampler::getRandomSample();
            double sqrtS1 = sqrt(sample.x());
            double alpha = 1 - sqrtS1;
            double beta = sqrtS1 * (1 - sample.y());
            double gamma = sqrtS1 * sample.y();


            unsigned triangleIndex = i * 9;
            Point v1 = vertices[triangles[triangleIndex]];
            Normal vn1 = vertexNormals[triangles[triangleIndex + 2]];
            Point v2 = vertices[triangles[triangleIndex + 3]];
            Normal vn2 = vertexNormals[triangles[triangleIndex + 5]];
            Point v3 = vertices[triangles[triangleIndex + 6]];
            Normal vn3 = vertexNormals[triangles[triangleIndex + 8]];

            Vector toP = v1 * alpha + v2 * beta + v3 * gamma;

            normal = t.applyInverseNormal((vn1 * alpha + vn2 * beta + vn3 * gamma).normalized());

            return t.translatePoint(Point(0, 0, 0)) + t * toP;
        }
    }

    return Point(); // This can never happen but it avoids a compiler warning
}

double MeshData::getSurfaceArea() const {
    return area;
}

AABoundingBox MeshData::getAABB() const {
    assert(bvh != nullptr);
    return bvh->getTopAABB();
}

