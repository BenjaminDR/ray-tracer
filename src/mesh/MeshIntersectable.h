#ifndef RAY_TRACER_MESHINTERSECTABLE_H
#define RAY_TRACER_MESHINTERSECTABLE_H


#include <Intersectable.h>
#include "MeshInstance.h"

class MeshIntersectable : public Intersectable {
public:
    MeshIntersectable(MeshData* m, int i): mesh(m), index(i) {};
    bool intersect(const Ray &ray, HitObject &hitObject, double maxDist) const override;
    [[nodiscard]] double distanceTo(const Ray &ray) const override;
    [[nodiscard]] bool encloses(const Point &p) const override;
    [[nodiscard]] double recursiveSurfaceArea() const override;

private:
    MeshData* mesh;
    int index;
};


#endif //RAY_TRACER_MESHINTERSECTABLE_H
