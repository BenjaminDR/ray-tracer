#ifndef RAY_TRACER_MESHDATA_H
#define RAY_TRACER_MESHDATA_H


#include <utility>
#include <Base.h>
#include <memory>
#include <acceleration/BVH.h>

class MeshData {

public:
    MeshData(std::vector<Point> verts, std::vector<Point2d> textCoords, std::vector<Normal> vertNormals,
            std::vector<int> triangs);

    MeshData(const MeshData &m) = delete;

    BVH *buildBVH(BVH_Partition_Strategy strategy, BVH_Axis_Order order);

    bool intersect(const Ray &ray, HitObject &hitObject, double maxDist, int triangleIndex) const;
    [[nodiscard]] double getSurfaceArea(int index) const;
    [[nodiscard]] double getSurfaceArea() const;
    AABoundingBox getAABB() const;


    Point getRandomSurfacePoint(const Point &visibleFrom, Normal &normal, const Transformation &t, double surfaceArea) const;

    std::vector<Point> vertices;
    std::vector<Point2d> textureCoords;
    std::vector<Normal> vertexNormals;
    std::vector<int> triangles;

private:
    std::unique_ptr<BVH> bvh;
    std::vector<double> triangleWeights;
    double area;
    std::vector<IntersectableInfo> info;
};

#endif //RAY_TRACER_MESHDATA_H
