#include "ProxyIntersectable.h"

bool ProxyIntersectable::intersect(const Ray &ray, HitObject &hitObject, double maxDist) const {
    Ray tRay = t / ray;
    bool intersect = intersectable->intersect(tRay, hitObject, maxDist);
    if (intersect) {
        hitObject.normal = t.applyInverseNormal(hitObject.normal);
        hitObject.hitPoint = ray.origin + ray.direction * hitObject.distance;
        hitObject.shadeable = shadeable;
    }
    return intersect;
}

double ProxyIntersectable::distanceTo(const Ray &ray) const {
    Ray tRay = t / ray;
    return intersectable->distanceTo(tRay);
}

bool ProxyIntersectable::encloses(const Point &p) const {
    Point transP = t.translatePoint(p);
    return intersectable->encloses(transP);
}

double ProxyIntersectable::recursiveSurfaceArea() const {
    return intersectable->recursiveSurfaceArea() * t.getScale().prod();
}

int ProxyIntersectable::interSectionCount(const Ray &ray, int depth, int minDepth, int maxDepth) const {
    Ray tRay = t / ray;
    return intersectable->interSectionCount(tRay, depth, minDepth, maxDepth);
}

ProxyIntersectable::ProxyIntersectable(Intersectable *i, Transformation trans, Shadeable *s) : intersectable(i),
    t(std::move(trans)), shadeable(s) {
}
