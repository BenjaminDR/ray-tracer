#include "MeshInstance.h"
#include "ProxyIntersectable.h"
#include <core/Primitive.h>
#include <film/Sampler.h>
#include <geometry/FlatTriangle.h>


MeshInstance::MeshInstance(std::shared_ptr<MeshData> d, Transformation trans) :
    data(std::move(d)), t(std::move(trans)) {}

bool MeshInstance::intersect(const Ray &ray, HitObject &hitObject, double maxDist, int triangleIndex) const {
    Ray tRay = t / ray;

    Point v1 = data->vertices[data->triangles[triangleIndex]];
    Point v2 = data->vertices[data->triangles[triangleIndex + 3]];
    Point v3 = data->vertices[data->triangles[triangleIndex + 6]];

    Vector edge1(v2 - v1);
    Vector edge2(v3 - v1);

    Vector h(tRay.direction.cross(edge2));

    double det = edge1.dot(h);

    if (det > -EPSILON && det < EPSILON) return false; // Ray is parallel to triangle

    double invDet = 1 / det;

    Vector s(tRay.origin - v1);

    double beta = invDet * s.dot(h);
    if (beta < 0 || beta > 1) return false;

    Vector q(s.cross(edge1));

    double gamma = invDet * tRay.direction.dot(q);
    if (gamma < 0 || beta + gamma > 1) return false;

    double T = invDet * edge2.dot(q);

    if(T < EPSILON || T > maxDist) return false;

    Normal vn1 = data->vertexNormals[data->triangles[triangleIndex + 2]];
    Normal vn2 = data->vertexNormals[data->triangles[triangleIndex + 5]];
    Normal vn3 = data->vertexNormals[data->triangles[triangleIndex + 8]];
    Point2d vt1 = data->textureCoords[data->triangles[triangleIndex + 1]];
    Point2d vt2 = data->textureCoords[data->triangles[triangleIndex + 4]];
    Point2d vt3 = data->textureCoords[data->triangles[triangleIndex + 7]];

    hitObject.distance = T;
    hitObject.normal = t.applyInverseNormal(((1 - beta - gamma) * vn1 + beta * vn2 + gamma * vn3).normalized());
    if(ray.direction.dot(hitObject.normal) > 0) hitObject.normal = -hitObject.normal;
    hitObject.hitPoint = ray.origin + T * ray.direction;
    hitObject.uv = vt1 * (1 - beta - gamma) + vt2 * beta + vt3 * gamma;

    return true;
}


std::unique_ptr<AABoundingBox> MeshInstance::getBoundingBox(int triangleIndex) const {
    Point origin = t.translatePoint(Point(0, 0, 0));
    Point v1 = t * data->vertices[data->triangles[triangleIndex + 0]] + origin;
    Point v2 = t * data->vertices[data->triangles[triangleIndex + 3]] + origin;
    Point v3 = t * data->vertices[data->triangles[triangleIndex + 6]] + origin;

    return std::make_unique<AABoundingBox>(v1.cwiseMin(v2).cwiseMin(v3), v1.cwiseMax(v2).cwiseMax(v3));
}


double MeshInstance::surfaceArea(int triangleIndex) const {
    Point v1 = data->vertices[data->triangles[triangleIndex]];
    Point v2 = data->vertices[data->triangles[triangleIndex + 3]];
    Point v3 = data->vertices[data->triangles[triangleIndex + 6]];

    Vector scale = t.getScale();

    Vector edge1(scale.cwiseProduct(v2 - v1));
    Vector edge2(scale.cwiseProduct(v3 - v1));

    return 0.5 * edge1.cross(edge2).norm();
}

double MeshInstance::surfaceArea() const {
    return data->getSurfaceArea() * t.getScale().prod();
}

Point MeshInstance::getRandomSurfacePoint(const Point &visibleFrom, Normal &normal) const {
    return data->getRandomSurfacePoint(visibleFrom, normal, t, surfaceArea());
}

void MeshInstance::addLight(Light *l) {
    light = l;
}

void MeshInstance::addMaterial(const std::shared_ptr<Material>& m) {
    material = m;
}

std::vector<std::shared_ptr<Primitive>> MeshInstance::createFlatPrimitives() const {
    std::vector<std::shared_ptr<Primitive>> primitives;
    primitives.reserve(data->triangles.size() / 9);

    Point origin = t.translatePoint(Point(0, 0, 0));

    for(unsigned i = 0u; i < data->triangles.size(); i += 9) {
        Point v1 = origin + t * data->vertices[data->triangles[i]];
        Point v2 = origin + t * data->vertices[data->triangles[i + 3]];
        Point v3 = origin + t * data->vertices[data->triangles[i + 6]];

        std::shared_ptr<Shape> shapePtr = std::make_shared<FlatTriangle>(v1, v2, v3);
        std::shared_ptr<Primitive> primPtr = std::make_shared<Primitive>(material, shapePtr, light);
        primitives.push_back(primPtr);
    }
    return primitives;
}

std::shared_ptr<Intersectable> MeshInstance::buildBVH(BVH_Partition_Strategy strategy, BVH_Axis_Order order) {
    BVH* meshBVH = data->buildBVH(strategy, order);
    return std::make_shared<ProxyIntersectable>(meshBVH, t, this);
}

AABoundingBox MeshInstance::getBoundingBox() const {
    return data->getAABB() * t;
}

const Light *MeshInstance::getLight() const {
    return light;
}

const Material *MeshInstance::getMaterial() const {
    return material.get();
}

