#ifndef RAY_TRACER_SHADER_H
#define RAY_TRACER_SHADER_H


#include <film/RGBSpectrum.h>

class Scene;
struct HitObject;

class Shader {

public:
    [[nodiscard]] RGBSpectrum virtual shade(const Scene &scene, const HitObject &hitObject, unsigned shadowRays) const;

};


#endif //RAY_TRACER_SHADER_H
