#include <math/Ray.h>
#include <Light.h>
#include <Material.h>
#include "Shader.h"
#include <HitObject.h>
#include <core/Scene.h>

RGBSpectrum Shader::shade(const Scene &scene, const HitObject &hitObject, unsigned shadowRays) const {
    RGBSpectrum shade, radiance;
    Ray shadowRay;
    const Light* selfLight = hitObject.shadeable->getLight();
    const Material* material = hitObject.shadeable->getMaterial();

    for(auto lightPtr : scene.getLights()) {
        if(lightPtr == selfLight) {
            shade += material->reflectance(hitObject) * lightPtr->Le();
            continue;
        }

        double pdf = lightPtr->pdf();
        if(pdf != 1) {
            for(unsigned i = 0; i < shadowRays; i++) {
                RGBSpectrum r;
                r += lightPtr->getRadiance(hitObject.hitPoint, hitObject.normal, shadowRay) / pdf;
                if(r.isZero()) continue;
                double maxShadowDistance = shadowRay.direction.norm();
                shadowRay.direction.normalize();
                if(scene.isIntersection(shadowRay, maxShadowDistance)) continue;
                radiance += r / shadowRays;
            }
            shade += material->reflectance(hitObject) * radiance;
            continue;
        }

        radiance = lightPtr->getRadiance(hitObject.hitPoint, hitObject.normal, shadowRay);
        if(radiance.isZero()) continue;
        if(shadowRays > 0) {
            double maxShadowDistance = shadowRay.direction.norm();
            shadowRay.direction.normalize();
            if(scene.isIntersection(shadowRay, maxShadowDistance)) continue;
        }
        shade += material->reflectance(hitObject) * radiance;
    }

    return shade;
//    return RGBSpectrum(abs(hitObject.normal.x()), abs(hitObject.normal.y()), abs(hitObject.normal.z()));
//    return RGBSpectrum(2/hitObject.distance) ^ 3;
}
