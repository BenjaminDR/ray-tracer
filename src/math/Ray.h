#ifndef RAY_TRACER_RAY_H
#define RAY_TRACER_RAY_H

#include <Base.h>

class Ray {

public:
    Ray();
    Ray(Point origin, Vector direction);
    Ray(const Ray &ray);
    Point origin;
    Vector direction;

    Ray operator-() {
        return Ray(origin, -direction);
    }

    friend std::ostream &operator<<(std::ostream &os, const Ray &ray);
};



#endif //RAY_TRACER_RAY_H
