#include "Ray.h"

#include <utility>

using namespace std;

Ray::Ray(Vector _origin, Vector  _direction): origin(std::move(_origin)),
    direction(std::move(_direction)){}

ostream &operator<<(ostream &os, const Ray &ray) {
    os << "Ray with origin: " << ray.origin.transpose() << " and direction: " << ray.direction.transpose() << endl;
    return os;
}

Ray::Ray() : Ray(Point(), Vector(0, 0, 0)) {}

Ray::Ray(const Ray &ray) : origin(ray.origin), direction(ray.direction) {}
