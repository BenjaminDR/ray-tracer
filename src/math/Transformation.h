#ifndef RAY_TRACER_TRANSFORMATION_H
#define RAY_TRACER_TRANSFORMATION_H

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <iostream>
#include "math/Ray.h"

class Transformation {

public:
    using AffineTrans = Eigen::Transform<double, 3, Eigen::Affine>;

    Transformation();
    explicit Transformation(const AffineTrans &m);
    Transformation(const AffineTrans& t_, const AffineTrans& tInv_);

    inline Transformation operator*(const Transformation &t2) const;
    inline Transformation operator/(const Transformation &t2) const;

    void translate(const Vector &delta);
    void scale(double x, double y, double z);
    void rotateX(double angle);
    void rotateY(double angle);
    void rotateZ(double angle);
    void rotate(const Vector &axis, double angle);

    [[nodiscard]] Normal applyInverseNormal(const Normal &normal) const;
    [[nodiscard]] Normal applyNormal(const Normal &normal) const;
    [[nodiscard]] Point translatePoint(const Point &point) const;
    [[nodiscard]] Point inverseTranslatePoint(const Point &point) const;

    [[nodiscard]] Vector getScale() const;

    Vector operator*(const Vector &vec) const {
        return t.linear() * vec;
    }
    Vector operator/(const Vector &vec) const {
        return tInv.linear() * vec;
    }

    Ray operator*(const Ray &ray) const {
        return {t * ray.origin, t.linear() * ray.direction };
    }
    Ray operator/(const Ray &ray) const {
        return {tInv * ray.origin, tInv.linear() * ray.direction };
    }

    const AffineTrans& get() const {
        return t;
    }

    friend std::ostream &operator<<(std::ostream &os, const Transformation &trans);

private:
    AffineTrans t;
    AffineTrans tInv;

};


Transformation Transformation::operator*(const Transformation &t2) const {
    return {t * t2.t , t2.tInv * tInv};
}

Transformation Transformation::operator/(const Transformation &t2) const {
    return {t * t2.tInv, t2.t * tInv};
}


#endif //RAY_TRACER_TRANSFORMATION_H
