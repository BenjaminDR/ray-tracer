#ifndef RAY_TRACER_ORTHONORMALBASIS_H
#define RAY_TRACER_ORTHONORMALBASIS_H

#include "Base.h"

class OrthonormalBasis {

public:
    OrthonormalBasis(Vector u_, Vector v_, Vector w_);
    const Vector u;
    const Vector v;
    const Vector w;

};


#endif //RAY_TRACER_ORTHONORMALBASIS_H
