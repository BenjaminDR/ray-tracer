#include "OrthonormalBasis.h"
#include <utility>

using namespace std;

OrthonormalBasis::OrthonormalBasis(Vector _u, Vector _v, Vector _w) : u(std::move(_u)), v(std::move(_v)), w(std::move(_w)){}
