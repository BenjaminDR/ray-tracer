#include "Transformation.h"

#include <iostream>

using namespace std;
using namespace Eigen;

Transformation::Transformation() : t(AffineTrans::Identity()), tInv(AffineTrans::Identity()) {}

Transformation::Transformation(const AffineTrans &t_) : t(t_), tInv(t_.inverse()) {}

Transformation::Transformation(const AffineTrans& t_, const AffineTrans& tInv_) : t(t_), tInv(tInv_) {}

void Transformation::translate(const Vector &delta) {
    t = Translation3d(delta) * t;
    tInv *= Translation3d(-delta);
}

void Transformation::scale(double x, double y, double z) {
    t = Scaling(x, y, z) * t;
    tInv *= Scaling(1/x, 1/y, 1/z);
}

void Transformation::rotateX(double angle) {
    t = AngleAxisd(angle * M_PI / 180, Vector(1, 0, 0)) * t;
    tInv *= AngleAxisd(angle * M_PI / 180, Vector(-1, 0, 0));
}

void Transformation::rotateY(double angle) {
    t = AngleAxisd(angle * M_PI / 180, Vector(0, 1, 0)) * t;
    tInv *= AngleAxisd(angle * M_PI / 180, Vector(0, -1, 0));
}

void Transformation::rotateZ(double angle) {
    t = AngleAxisd(angle * M_PI / 180, Vector(0, 0, 1)) * t;
    tInv *= AngleAxisd(angle * M_PI / 180, Vector(0, 0, -1));
}

void Transformation::rotate(const Vector &axis, double angle) {
    t = AngleAxisd(angle * M_PI / 180, axis) * t;
    tInv *= AngleAxisd(angle * M_PI / 180, -axis);
}


ostream &operator<<(ostream &os, const Transformation &trans) {
    os  << "Transformation matrix:" << endl
        << trans.t.matrix() << endl
        << "Inverse transformation matrix:" << endl
        << trans.tInv.matrix() << endl;
    return os;
}

Normal Transformation::applyInverseNormal(const Normal &normal) const {
    return (tInv.linear().transpose() * normal).normalized();
}

Normal Transformation::applyNormal(const Normal &normal) const {
    return (t.linear().transpose() * normal).normalized();
}

Point Transformation::translatePoint(const Point &point) const {
    return Point(t * point);
}

Vector Transformation::getScale() const {
    Eigen::Matrix3d lin = t.linear();
    return Vector(lin(0, 0), lin(1, 1), lin(2, 2));

}

Point Transformation::inverseTranslatePoint(const Point &point) const {
    return Point(tInv * point);
}


