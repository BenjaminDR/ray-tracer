#include "PerspectiveCamera.h"

#include <utility>
#include <iostream>
#include <Eigen/Geometry>

using namespace std;

PerspectiveCamera::PerspectiveCamera(int xResolution, int yResolution, Vector origin_, const Vector &lookat,
        const Vector &up, double fov) :
        origin(std::move(origin_)),
        basis(PerspectiveCamera::createOrthonormalBasis(-lookat, up)) {
    invXResolution = 1.0 / xResolution;
    invYResolution = 1.0 / yResolution;
    width = 2.0 * tan(0.5 * fov * M_PI / 180);
    height = yResolution * width * invXResolution;
}

Ray PerspectiveCamera::generateRay(const Sample &sample) const {
    double u = width * (sample.x() * invXResolution - 0.5);
    double v = height * (sample.y() * invYResolution - 0.5);

    Vector direction = basis.u * u + basis.v * v - basis.w;
    return {origin, direction.normalized()};
}

OrthonormalBasis PerspectiveCamera::createOrthonormalBasis(const Vector &negLookat, const Vector &up) {
    Vector u = up.cross(negLookat);

    // Check for colinearity between vectors a and b
    float colinearityFactor = u.size();
    assert(colinearityFactor != 0);
    if (colinearityFactor < 1e-8)
        cerr << "Warning: Orthonormal basis construction parameters are almost colinear" << endl
             << negLookat << endl << up << endl;

    u.normalize();
    Vector w = negLookat.normalized();
    Vector v = w.cross(u);

    return {u, v, w};
}


OrthonormalBasis PerspectiveCamera::createOrthonormalBasis(const Vector &a) {
    Vector w = a.normalized();
    Vector u;
    if(abs(w.x()) > abs(w.y())) u = w.cross(Vector(0,1, 0));
    else u = w.cross(Vector(1, 0, 0));
    u.normalize();
    Vector v = w.cross(u);

    return {u, v, w};
}

Camera *
PerspectiveCamera::create(int xResolution, int yResolution, Vector _origin, const Vector &lookat, const Vector &up,
                          double fov) const {
    return new PerspectiveCamera(xResolution, yResolution, _origin, lookat, up, fov);
}

Camera *PerspectiveCamera::clone() const {
    return new PerspectiveCamera(*this);
}

