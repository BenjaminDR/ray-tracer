#ifndef RAY_TRACER_PERSPECTIVECAMERA_H
#define RAY_TRACER_PERSPECTIVECAMERA_H


#include "math/Ray.h"
#include "Camera.h"
#include "math/OrthonormalBasis.h"

class PerspectiveCamera : public Camera {

public:
    PerspectiveCamera(int xResolution, int yResolution, Vector origin_, const Vector &lookat,
            const Vector &up, double fov);
//    PerspectiveCamera(PerspectiveCamera const &);
    [[nodiscard]] Camera * create(int xResolution, int yResolution, Vector _origin, const Vector &lookat,
                    const Vector &up, double fov) const override;
    [[nodiscard]] Camera * clone() const override;

    [[nodiscard]] Ray generateRay(const Sample &sample) const override;

private:
    Vector origin;
    const OrthonormalBasis basis;
    double width, height, invXResolution, invYResolution;
    static OrthonormalBasis createOrthonormalBasis(const Vector &negLookat, const Vector &up);
    static OrthonormalBasis createOrthonormalBasis(const Vector &a);

};


#endif //RAY_TRACER_PERSPECTIVECAMERA_H
