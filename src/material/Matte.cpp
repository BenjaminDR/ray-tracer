#include "Matte.h"
#include <core/Scene.h>

Matte::Matte(const RGBSpectrum &color) : diffuseBRDF(Lambertian(color, 1)) {}

RGBSpectrum Matte::reflectance(const HitObject &hitObject) const {
    return diffuseBRDF.evaluate();
}

