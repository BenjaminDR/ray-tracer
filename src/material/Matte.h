#ifndef RAY_TRACER_MATTE_H
#define RAY_TRACER_MATTE_H


#include "Material.h"
#include "Lambertian.h"
#include "HitObject.h"

class Matte : public Material {

public:
    explicit Matte(const RGBSpectrum &color);

    [[nodiscard]] RGBSpectrum reflectance(const HitObject &hitObject) const override;

private:
    const Lambertian diffuseBRDF;
};


#endif //RAY_TRACER_MATTE_H
