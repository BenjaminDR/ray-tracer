#ifndef RAY_TRACER_TEXTURED_H
#define RAY_TRACER_TEXTURED_H


#include <Material.h>
#include <mesh/MeshInstance.h>
#include <film/Pixel.h>

class Textured : public Material {

public:
    explicit Textured(const std::string& textureName);
    [[nodiscard]] RGBSpectrum reflectance(const HitObject &hitObject) const override;

private:
    Pixel* pexels;
    int x, y, n;
    float gamma = 2.2; // TODO: Link to variable in main
};


#endif //RAY_TRACER_TEXTURED_H
