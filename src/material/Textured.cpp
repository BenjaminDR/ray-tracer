#include "Textured.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Textured::Textured(const std::string& textureName) {
    unsigned char *data = stbi_load(("textures/" + textureName).c_str(), &x, &y, &n, 3);
    pexels = new Pixel[x * y];
    for(int j = 0; j < y; j++) {
        for(unsigned i = 0; i < x; i++) {
            unsigned char *p = &data[(i + j * x) * n];
            pexels[i + ((y - 1 - j) * x)] = Pixel((RGBSpectrum(p[0], p[1], p[2]) / 255) ^ gamma);
        }
    }
    stbi_image_free(data);
}

RGBSpectrum Textured::reflectance(const HitObject &hitObject) const {
    Vector2 uv = hitObject.uv.cwiseProduct(Vector2((x - 1), (y - 1)));
    double uRem = uv.x() - floor(uv.x());
    double vRem = uv.y() - floor(uv.y());
    RGBSpectrum tex1 = pexels[(int) (floor(uv[0]) + x * floor(uv[1]))].getSpectrum() * (1 - uRem) * (1 - vRem);
    RGBSpectrum tex2 = pexels[(int) (floor(uv[0]) + x * ceil(uv[1]))].getSpectrum() * (1 - uRem) * vRem;
    RGBSpectrum tex3 = pexels[(int) (ceil(uv[0]) + x * floor(uv[1]))].getSpectrum() * uRem * (1 - vRem);
    RGBSpectrum tex4 = pexels[(int) (ceil(uv[0]) + x * ceil(uv[1]))].getSpectrum() * uRem * vRem;
    return tex1 + tex2 + tex3 + tex4;
}

