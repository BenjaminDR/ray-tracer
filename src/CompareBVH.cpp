#include <iostream>
#include <core/Scene.h>
#include <chrono>
#include "Stats.h"
#include "acceleration/BVH.h"
#include "core/SceneGenerator.h"

STAT_INIT_THREADED(BBintersects, BBintersects_l)
STAT_INIT_THREADED(Shapeintersects, Shapeintersects_l)

int main(int argc, char* argv[]) {
    if(argc < 2) {
        std::cout << "Invalid amount of arguments...\n\n"
                  << "Correct usage: compare_bvh [testName]\n"
                  << "Options: Build [0|1|2], Instancing-memory [n], TriangleSoup-memory [n], Render\n";
        exit(1);
    }

    std::string testArg = argv[1];
    if(testArg == "Build" && argc == 3) {
        BVH_Partition_Strategy strategies[] = {
                BVH_Partition_Strategy::OBJECT_MEDIAN_SPLIT,
                BVH_Partition_Strategy::SPATIAL_MEDIAN_SPLIT,
                BVH_Partition_Strategy::SURFACE_AREA_HEURISTIC,
        };
        Generation_Spread spreads[] = {
                Generation_Spread::Regular,
                Generation_Spread::UniformlyDistributed,
                Generation_Spread::NormallyDistributed,
                Generation_Spread::ExponentiallyDistributed,
        };
        unsigned repeat = 1;
        const int max = 5000000;
        const int dataPoints = 20;
        const int step = max / dataPoints;
        int s = std::stoi(argv[2]);

        std::cout << "Primitives, Object median split, Spatial median split, Surface area heuristic\n"
                  << "0, 0, 0, 0\n";
        const FrameBuffer buf = FrameBuffer(500,500);

        for (unsigned i = 5; i <= max; i *= 2) {
//            if (i < 1000) repeat = 100000 / i;
//            else repeat = 100;
//            for (unsigned i = step; i <= max; i += step) {
            Scene scene;
            SceneGenerator::generateMeshScene(scene, Generation_Mesh::Venus, i, spreads[s], 0);

            std::cout << i << ", ";
//            std::cout << (double)i / 1000 << ", ";
            for (int k = 0; k < 2; k++) {
                double elapsed = 0;
                for (unsigned j = 0; j < repeat; j++) {
                    if(k == 1) scene.buildTriangleSoup();
                    scene.buildBVH(BVH_Partition_Strategy::SURFACE_AREA_HEURISTIC, BVH_Axis_Order::DOMINANT);
                    auto start = std::chrono::steady_clock::now();
                    SceneGenerator::generateCamera(scene, 500, 500);
                    scene.render(buf, 1, 0, false);
                    elapsed += (double) (std::chrono::steady_clock::now() - start).count() / 1000;
                    scene.removeBVH();
                }
                std::cout << elapsed / repeat << ", ";
            }
            std::cout << std::endl;
        }
    } else if(testArg == "Instancing-memory" && argc == 3){
        int n = std::stoi(argv[2]);
        Scene scene;
        SceneGenerator::generateMeshScene(scene, Generation_Mesh::Venus, n, Generation_Spread::Regular, 0);
        scene.buildBVH(BVH_Partition_Strategy::SURFACE_AREA_HEURISTIC, BVH_Axis_Order::DOMINANT);
    } else if(testArg == "TriangleSoup-memory" && argc == 3){
        int n = std::stoi(argv[2]);
        Scene scene;
        SceneGenerator::generateMeshScene(scene, Generation_Mesh::Venus, n, Generation_Spread::Regular, 0);
        scene.buildTriangleSoup();
    } else if(testArg == "Render" && argc == 3) {
        BVH_Partition_Strategy strategies[] = {
                BVH_Partition_Strategy::OBJECT_MEDIAN_SPLIT,
                BVH_Partition_Strategy::SPATIAL_MEDIAN_SPLIT,
                BVH_Partition_Strategy::SURFACE_AREA_HEURISTIC,
        };
        Generation_Spread spreads[] = {
                Generation_Spread::Regular,
                Generation_Spread::UniformlyDistributed,
                Generation_Spread::NormallyDistributed,
                Generation_Spread::ExponentiallyDistributed,
        };
        const unsigned repeat = 100;
        int s = std::stoi(argv[2]);

        std::cout << "Primitives, Object median split, Spatial median split, Surface area heuristic\n";

        FrameBuffer buffer(500, 500);

        for (unsigned i = 5; i <= 1000000; i *= 2) {

            std::cout << i << ", ";
            for (auto &strategy : strategies) {
                STAT_OP(BBintersects = 0;)
                STAT_OP(Shapeintersects = 0;)
//                double cost = 0;
                for (unsigned j = 0; j < repeat; j++) {
                    Scene scene;
                    SceneGenerator::generateShapeScene(scene, Generation_Shape::Sphere, i, spreads[s], 0);
                    scene.buildBVH(strategy, BVH_Axis_Order::DOMINANT);
//                    cost += scene.evaluateBVH();
                    SceneGenerator::generateCamera(scene, 500, 500);
                    scene.render(buffer, 1, 0, false);
                    scene.removeBVH();
                }
                STAT_OP(std::cout << double (BBintersects + Shapeintersects) / repeat / 500.0 / 500.0 << ",";)
//                std::cout << cost / repeat << ",";
            }
            std::cout << std::endl;
        }
    } else if(testArg == "Temp") {
        Scene scene;
        SceneGenerator::generateShapeScene(scene, Generation_Shape::Sphere, 5000000, Generation_Spread::UniformlyDistributed, 0);

        std::cout << "Start building...\n";
        auto start = std::chrono::steady_clock::now();
        scene.buildBVH(BVH_Partition_Strategy::OBJECT_MEDIAN_SPLIT, BVH_Axis_Order::DOMINANT);
        auto elapsed = (double) (std::chrono::steady_clock::now() - start).count() / 1000000000;
        scene.removeBVH();
        std::cout << elapsed << "\n";
    } else {
        std::cout << "Test name not recognised, please specify the correct test.\n"
                  << "Options: Build, Instancing-memory [n], TriangleSoup-memory [n], Render\n";
        exit(1);
    }

    return 0;
}
