#include <iostream>
#include "Sampler.h"
#include <thread>

static thread_local std::default_random_engine re;
static thread_local std::uniform_real_distribution<double> dist(0, 1);
static thread_local bool seeded = false;

#pragma ide diagnostic ignored "cert-flp30-c"
std::vector<Sample> Sampler::generateSamples(unsigned x, unsigned y, unsigned sqrtSpp, SamplingMethod method) {
    if(!seeded) generateSeed();

    std::vector<Sample> samples;
    samples.reserve(sqrtSpp * sqrtSpp);

    if(method == SamplingMethod::REGULAR) {
        for(double sx = x + 1.0 / (2 * sqrtSpp); sx < x + 1; sx += 1.0/sqrtSpp)
            for(double sy = y + 1.0 / (2 * sqrtSpp); sy < y + 1; sy += 1.0/sqrtSpp)
                samples.emplace_back(sx, sy);
    }
    else if(method == SamplingMethod::RANDOM) {
        for (unsigned i = 0; i < sqrtSpp * sqrtSpp; i++) samples.emplace_back(x + dist(re), y + dist(re));
    } else if(method == SamplingMethod::JITTERED) {
        for(unsigned sx = 0; sx < sqrtSpp; sx++)
            for(unsigned sy = 0; sy < sqrtSpp; sy++)
                samples.emplace_back(x + 1.0/sqrtSpp * (sx + dist(re)), y + 1.0/sqrtSpp * (sy + dist(re)));
    } else {
        std::cerr << "Sampling method is not supported.\n";
    }

    return samples;
}

void Sampler::generateSeed() {
    unsigned threadID = std::hash<std::thread::id>{}(std::this_thread::get_id());
    re.seed((threadID + 1) * time(nullptr));
    seeded = true;
}

Sample Sampler::getRandomSample() {
    if(!seeded) generateSeed();
    return Sample(dist(re), dist(re));
}

double Sampler::getRandomDouble() {
    if(!seeded) generateSeed();
    return dist(re);
}
