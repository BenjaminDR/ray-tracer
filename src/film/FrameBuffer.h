#ifndef RAY_TRACER_FRAMEBUFFER_H
#define RAY_TRACER_FRAMEBUFFER_H


#include <string>
#include <vector>
#include "RGBSpectrum.h"
#include "Pixel.h"

class FrameBuffer {

public:
    FrameBuffer(int xRes, int yRes);
    ~FrameBuffer();

    [[nodiscard]] Pixel& getPixel(unsigned x, unsigned y) const;
    [[nodiscard]] Pixel& getPixel(unsigned x) const;
    void saveToPPM(const std::string &filename, double gamma, double sensitivity);

    int xRes, yRes;
private:
    Pixel* pixels;

};


#endif //RAY_TRACER_FRAMEBUFFER_H
