#include <fstream>
#include <iostream>
#include "FrameBuffer.h"

using namespace std;

FrameBuffer::FrameBuffer(int _xRes, int _yRes) : xRes(_xRes), yRes(_yRes){
    pixels = new Pixel[xRes * yRes];
}

void FrameBuffer::saveToPPM(const std::string &filename, double gamma, double sensitivity) {

    double invGamma = 1 / gamma;
    double invSensitivity = 1 / sensitivity;

    ofstream outputFile(filename + ".ppm", ios_base::out);
    // Set ppm headers
    outputFile << "P3\n" << xRes << " " << yRes << "\n255\n";

    for (int i = 0; i < xRes * yRes; i++) {
        RGBSpectrum color = pixels[i].getSpectrum();
        color = ((color.clamp(0, invSensitivity) * sensitivity) ^ invGamma) * 255;
        outputFile << round(color.red) << " " << round(color.green) << " " << round(color.blue) << "\n";
    }
}

Pixel &FrameBuffer::getPixel(unsigned x, unsigned y) const {
    return pixels[y * xRes + x];
}

FrameBuffer::~FrameBuffer() {

    delete[](pixels);

}

Pixel &FrameBuffer::getPixel(unsigned x) const {
    return pixels[x];
}

