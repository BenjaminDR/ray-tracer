#ifndef RAY_TRACER_PIXEL_H
#define RAY_TRACER_PIXEL_H


#include "RGBSpectrum.h"

class Pixel {

public:
    Pixel();
    Pixel(const RGBSpectrum &spectrum);
    void add(double red, double green, double blue, double weight);
    void add(double red, double green, double blue);
    void add(RGBSpectrum spectrum, double weight);
    RGBSpectrum getSpectrum();

private:
    RGBSpectrum color;
    double weightSum = 0;

};


#endif //RAY_TRACER_PIXEL_H
