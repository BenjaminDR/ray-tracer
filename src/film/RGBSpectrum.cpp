#include <Base.h>
#include "RGBSpectrum.h"


RGBSpectrum::RGBSpectrum() : RGBSpectrum(0) {}

RGBSpectrum::RGBSpectrum(double value) : RGBSpectrum(value, value, value) {}

RGBSpectrum::RGBSpectrum(double _red, double _green, double _blue) : red(_red), green(_green), blue(_blue) {}

RGBSpectrum RGBSpectrum::clamp(double low, double high) const {
    Vector thisRGB(red, green, blue);
    Vector lowV(low, low, low);
    Vector highV(high, high, high);
    Vector clamped {thisRGB.cwiseMax(lowV).cwiseMin(highV)};
    return RGBSpectrum(clamped.x(), clamped.y(), clamped.z());
}

uint32 RGBSpectrum::toRGB() {
    RGBSpectrum clamped { clamp(0, 255)};
    return (255u << 24u) + ((uint8) round(red) << 16u) + ((uint8) round(green) << 8u) + (uint8) round(blue);
}

RGBSpectrum RGBSpectrum::add(double _red, double _green, double _blue) const {
    return RGBSpectrum(red + _red, green + _green, blue + _blue);
}

bool RGBSpectrum::isZero() {
    return red == 0 && green == 0 && blue == 0;
}

