#ifndef RAY_TRACER_RGBSPECTRUM_H
#define RAY_TRACER_RGBSPECTRUM_H

#include <Eigen/Core>
#include <tiff.h>

class RGBSpectrum {

public:
    explicit RGBSpectrum();
    explicit RGBSpectrum(double value);
    explicit RGBSpectrum(double red, double green, double blue);
    [[nodiscard]] RGBSpectrum add(double red, double green, double blue) const;
    [[nodiscard]] RGBSpectrum clamp(double low, double high) const;
    uint32 toRGB();
    bool isZero();

    double red, green, blue;

    RGBSpectrum operator*(double scalar) const {
        return RGBSpectrum(red * scalar, green * scalar, blue * scalar);
    }

    RGBSpectrum operator/(double scalar) const {
        assert(scalar != 0);
        return RGBSpectrum(red / scalar, green / scalar, blue / scalar);
    }

    RGBSpectrum operator^(double scalar) const {
        return RGBSpectrum(pow(red, scalar), pow(green, scalar), pow(blue, scalar));
    }


    RGBSpectrum operator+(const RGBSpectrum &spectrum) const {
        return RGBSpectrum(red + spectrum.red, green + spectrum.green, blue + spectrum.blue);
    }
    RGBSpectrum &operator+=(const RGBSpectrum &spectrum) {
        red += spectrum.red;
        green += spectrum.green;
        blue += spectrum.blue;
        return *this;
    }

    RGBSpectrum operator*(const RGBSpectrum &spectrum) const {
        return RGBSpectrum(red * spectrum.red, green * spectrum.green, blue * spectrum.blue);
    }

    inline static RGBSpectrum fromRGB(unsigned red, unsigned green, unsigned blue) {
        float gamma = 2.2; // TODO: link aan gamma in main
        return (RGBSpectrum(red, green, blue) / 255) ^ 2.2;
    }

};


#endif //RAY_TRACER_RGBSPECTRUM_H
