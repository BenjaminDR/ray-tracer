#ifndef RAY_TRACER_SAMPLER_H
#define RAY_TRACER_SAMPLER_H

#include <vector>
#include <random>
#include "Base.h"

enum class SamplingMethod {
    REGULAR,
    RANDOM,
    JITTERED
};

class Sampler {

public:
    static std::vector<Sample> generateSamples(unsigned x, unsigned y, unsigned sqrtSpp, SamplingMethod method);
    static Sample getRandomSample();
    static double getRandomDouble();

    static void generateSeed();

};


#endif //RAY_TRACER_SAMPLER_H
