#include "Pixel.h"

Pixel::Pixel() : color(RGBSpectrum()) {}

Pixel::Pixel(const RGBSpectrum &spectrum) : color(spectrum) {}

void Pixel::add(double red, double green, double blue, double weight) {
    color = color.add(red * weight, green * weight, blue * weight);
    weightSum += weight;
}

void Pixel::add(double red, double green, double blue) {
    add(red, green, blue, 1);
}

RGBSpectrum Pixel::getSpectrum() {
    if(weightSum == 0) return color;
    return color * (1/weightSum);
}

void Pixel::add(RGBSpectrum spectrum, double weight) {
    color += (spectrum * weight);
    weightSum += weight;
}



