#include "Lambertian.h"


Lambertian::Lambertian(const RGBSpectrum &_color, double _reflectionCoefficient) :
    BRDF(_color),
    reflectionCoefficient(_reflectionCoefficient) {}

RGBSpectrum Lambertian::evaluate() const {
    return color * reflectionCoefficient;
}
