#ifndef RAY_TRACER_BRDF_H
#define RAY_TRACER_BRDF_H

#include "film/RGBSpectrum.h"
#include "Base.h"

class BRDF {

public:
    explicit BRDF(const RGBSpectrum &_color) : color(_color) {};
    [[nodiscard]] virtual RGBSpectrum evaluate() const = 0;

protected:
    RGBSpectrum color;
};

#endif //RAY_TRACER_BRDF_H
