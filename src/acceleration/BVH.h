#ifndef RAY_TRACER_BVH_H
#define RAY_TRACER_BVH_H


#include <memory>
#include "AABoundingBox.h"
#include "core/Primitive.h"

enum class BVH_Partition_Strategy {
    SPATIAL_MEDIAN_SPLIT,
    OBJECT_MEDIAN_SPLIT,
    SURFACE_AREA_HEURISTIC
};

enum class BVH_Axis_Order {
    DOMINANT,
    SEQUENTIAL,
    BEST
};

struct IntersectableInfo {
    std::shared_ptr<Intersectable> intersectable;
    AABoundingBox boundingBox;
    Point centroid;
};

class BVH : public Intersectable {

public:
    BVH() = default;
    explicit BVH(int sAxis);
    void
    build(IntersectableInfo *intersectableInfo[], unsigned infoLength, unsigned depth, BVH_Partition_Strategy strategy,
          BVH_Axis_Order order);
    void clear();
    bool intersect(const Ray &ray, HitObject &hitObject, double maxDist) const override;
    [[nodiscard]] double distanceTo(const Ray &ray) const override;
    [[nodiscard]] bool encloses(const Point &p) const override;
    [[nodiscard]] double recursiveSurfaceArea() const override;
    [[nodiscard]] double evaluateCost() const;
    [[nodiscard]] bool isBuilt() const;
    [[nodiscard]] const AABoundingBox& getTopAABB() const;

    [[nodiscard]] int interSectionCount(const Ray &ray, int depth, int minDepth, int maxDepth) const override;

private:
    unsigned partition(BVH_Partition_Strategy strategy, IntersectableInfo **intersectableInfo, unsigned infoLength,
                       unsigned axis, const AABoundingBox &centroidBox);
    static unsigned partitionSpatialMedianSplit(IntersectableInfo* intersectableInfo[], unsigned infoLength, unsigned axis,
                                const AABoundingBox &centroidBox);
    static unsigned partitionObjectMedianSplit(IntersectableInfo* intersectableInfo[], unsigned infoLength, unsigned axis);

    unsigned partitionSurfaceAreaHeuristic(IntersectableInfo* intersectableInfo[], unsigned infoLength, unsigned axis,
                                           const AABoundingBox &centroidBox);
    unsigned chooseSplittingAxis(const AABoundingBox &centroidBox, BVH_Axis_Order order, BVH_Partition_Strategy strat,
                                 IntersectableInfo *info[], unsigned infoLength);

    std::shared_ptr<Intersectable> left;
    std::shared_ptr<Intersectable> right;
    AABoundingBox boundingBox;
    int sortedAxis = -1;
};


#endif //RAY_TRACER_BVH_H
