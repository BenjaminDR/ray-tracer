#include "BVH.h"
#include "Stats.h"

STAT_INIT(max_bvh_depth)
STAT_INIT(min_bvh_depth)

BVH::BVH(int sAxis) : sortedAxis(sAxis) {}

void BVH::build(IntersectableInfo *intersectableInfo[], unsigned infoLength, unsigned depth,
                BVH_Partition_Strategy strategy, BVH_Axis_Order order) {

    STAT_OP(min_bvh_depth = 10000)

    AABoundingBox centroidBox;
    for(unsigned i = 0; i < infoLength; i++) {
        boundingBox += intersectableInfo[i]->boundingBox;
        centroidBox += intersectableInfo[i]->centroid;
    }

    if(infoLength == 2) {
        assert(intersectableInfo[0]->intersectable != nullptr);
        assert(intersectableInfo[1]->intersectable != nullptr);
        left = intersectableInfo[0]->intersectable;
        right = intersectableInfo[1]->intersectable;
        STAT_OP(max_bvh_depth = std::max(max_bvh_depth, depth))
        STAT_OP(min_bvh_depth = std::min(min_bvh_depth, depth))
        return;
    }

    // Get axis to split on
    unsigned axis = chooseSplittingAxis(centroidBox, order, strategy, intersectableInfo, infoLength);

    // Partition
    auto middle = partition(strategy, intersectableInfo, infoLength, axis, centroidBox);

    // Left node
    if(middle == 1) left = intersectableInfo[0]->intersectable;
    else {
        std::shared_ptr<BVH> leftNode = std::make_shared<BVH>(axis);
        leftNode->build(intersectableInfo, middle, depth + 1, strategy, order);
        left = leftNode;
    }

    // Right node
    if(infoLength - middle == 1) right = intersectableInfo[middle]->intersectable;
    else {
        std::shared_ptr<BVH> rightNode = std::make_shared<BVH>(axis);
        rightNode->build(intersectableInfo + middle, infoLength - middle, depth + 1, strategy, order);
        right = rightNode;
    }
}

unsigned BVH::partitionSpatialMedianSplit(IntersectableInfo* intersectableInfo[], unsigned infoLength, unsigned axis,
                                          const AABoundingBox &centroidBox) {
    double splitAt = centroidBox.getCentroid()(axis);

    // Partition
    auto pivot = std::partition(intersectableInfo, intersectableInfo + infoLength,
        [splitAt, axis](const IntersectableInfo* info){ return info->centroid(axis) < splitAt; });
    unsigned middle = pivot - intersectableInfo;

    return middle;
}

unsigned BVH::partitionObjectMedianSplit(IntersectableInfo* intersectableInfo[], unsigned infoLength, unsigned axis) {
    std::nth_element(intersectableInfo, intersectableInfo + infoLength / 2, intersectableInfo + infoLength,
                     [axis](const IntersectableInfo* a, const IntersectableInfo* b){
         return a->centroid(axis) < b->centroid(axis);
    });
    return infoLength / 2;
}

unsigned BVH::partitionSurfaceAreaHeuristic(IntersectableInfo* intersectableInfo[], unsigned infoLength, unsigned axis,
                                            const AABoundingBox &centroidBox) {
    const unsigned bucketCount = 16;

    double width = centroidBox.getWidthForAxis(axis);
    if(centroidBox.isZero() || width == 0 ) return partitionObjectMedianSplit(intersectableInfo, infoLength, axis);

    unsigned primsInBucket[bucketCount] = {};
    AABoundingBox boxInBucket[bucketCount];

    double minBound = centroidBox.getMinForAxis(axis);
    double invWidthPerBucket = bucketCount / width;
    for(unsigned i = 0; i < infoLength; i++) {
        int index = (int) ((intersectableInfo[i]->centroid[axis] - minBound) * invWidthPerBucket);
        index = std::min(index, (int) (bucketCount - 1));
        primsInBucket[index]++;
        boxInBucket[index] += intersectableInfo[i]->centroid;
    }

    AABoundingBox rightBox[bucketCount - 1];
    rightBox[bucketCount - 2] = boxInBucket[bucketCount - 1];
    for(int i = (int)bucketCount - 3; i >= 0; i--) rightBox[i] = rightBox[i + 1] + boxInBucket[i + 1];

    AABoundingBox leftBox = boxInBucket[0];
    unsigned leftPrims = primsInBucket[0];
    unsigned totalPrims = infoLength;
    double minCost = DINF;
    double split = rightBox[0].getMinForAxis(axis);
    for(unsigned i = 0; i < bucketCount - 2; i ++) {
        double cost = ( leftPrims * leftBox.getHalfSurfaceArea() + (totalPrims - leftPrims) *
                rightBox[i].getHalfSurfaceArea()) / boundingBox.getHalfSurfaceArea();
        if(cost < minCost) {
            minCost = cost;
            split = rightBox[i].getMinForAxis(axis);
        }
        leftBox += boxInBucket[i + 1];
        leftPrims += primsInBucket[i + 1];
    }

    auto pivot = std::partition(intersectableInfo, intersectableInfo + infoLength,
            [split, axis](const IntersectableInfo* info){ return info->centroid(axis) < split; });

    assert(pivot - intersectableInfo != 0);
    assert(pivot - intersectableInfo != infoLength);
    return pivot - intersectableInfo;
}

double BVH::evaluateCost() const {
    return 1 + (left->recursiveSurfaceArea() + right->recursiveSurfaceArea()) / boundingBox.surfaceArea();
}

bool BVH::intersect(const Ray &ray, HitObject &hitObject, double maxDist) const {
    // Calculate distance to children
    double leftDist = left->distanceTo(ray);
    double rightDist = right->distanceTo(ray);

    // Early abort checks
    if (leftDist == DINF && rightDist == DINF) return false;
    if (maxDist < DINF && leftDist > maxDist && rightDist > maxDist) return false;

    // Map to closest / furthest
    Intersectable *closest;
    Intersectable *furthest;
    double furthestDist;

    if (leftDist <= rightDist) {
        closest = left.get();
        furthest = right.get();
        furthestDist = rightDist;
    } else {
        closest = right.get();
        furthest = left.get();
        furthestDist = leftDist;
    }

    // First intersect closest
    bool intersection = closest->intersect(ray, hitObject, maxDist);

    // Early abort
    if (furthestDist == DINF) return intersection;

    // Only check furthest in right conditions
    if (!intersection || furthestDist < hitObject.distance || furthest->encloses(hitObject.hitPoint))
        intersection = furthest->intersect(ray, hitObject, std::min(maxDist, hitObject.distance)) || intersection;

    // Update bvhDepth
    if(intersection) hitObject.bvhDepth++;

    return intersection;
}

bool BVH::isBuilt() const {
    return left != nullptr && right != nullptr;
}

void BVH::clear() {
    left = nullptr;
    right = nullptr;
}

const AABoundingBox &BVH::getTopAABB() const {
    return boundingBox;
}

double BVH::distanceTo(const Ray &ray) const {
    HitObject o;
    boundingBox.intersect(ray, o, DINF);
    return o.distance;
}

bool BVH::encloses(const Point &p) const {
    return boundingBox.encloses(p);
}

double BVH::recursiveSurfaceArea() const {
    return boundingBox.surfaceArea() + left->recursiveSurfaceArea() + right->recursiveSurfaceArea();
}

int BVH::interSectionCount(const Ray &ray, int depth, int minDepth, int maxDepth) const {
    HitObject h;
    if(depth > maxDepth) return 0;
    if(boundingBox.intersect(ray, h, DINF)) {
        int currCount = depth < minDepth ? 0 : 1;
        return currCount + left->interSectionCount(ray, depth + 1, minDepth, maxDepth) +
               right->interSectionCount(ray, depth + 1, minDepth, maxDepth);
    }
    return 0;
}

unsigned BVH::chooseSplittingAxis(const AABoundingBox &centroidBox, BVH_Axis_Order order, BVH_Partition_Strategy strat,
                                  IntersectableInfo *info[], unsigned infoLength) {
    if(order == BVH_Axis_Order::DOMINANT) return centroidBox.getDominantAxis();
    if (order == BVH_Axis_Order::SEQUENTIAL) return (sortedAxis + 1) % 3;
    if (order == BVH_Axis_Order::BEST) {
        int minAxis = 0;
        double minScore = DINF;
        for(int u = 0; u < 3; u++) {
            auto testInfo = new IntersectableInfo*[infoLength];
            memcpy(testInfo, info, sizeof(IntersectableInfo*) * infoLength);

            auto middle = partition(strat, testInfo, infoLength, u, centroidBox);
            AABoundingBox leftBox, rightBox;
            for (int i = 0; i < middle; ++i) {
                leftBox += testInfo[i]->boundingBox;
            }
            for (int i = middle; i < infoLength; ++i) {
                rightBox += testInfo[i]->boundingBox;
            }
            double score = middle * leftBox.getHalfSurfaceArea() + (infoLength - middle) * rightBox.getHalfSurfaceArea();
            if(score < minScore) {
                minScore = score;
                minAxis = u;
            }

            delete [] testInfo;
        }
        return minAxis;
    }
    std::cerr << "Axis order not yet implemented";
    exit(1);
}

unsigned BVH::partition(BVH_Partition_Strategy strategy, IntersectableInfo **intersectableInfo, unsigned infoLength,
                        unsigned axis, const AABoundingBox &centroidBox) {
    if(strategy == BVH_Partition_Strategy::SPATIAL_MEDIAN_SPLIT)
        return partitionSpatialMedianSplit(intersectableInfo, infoLength, axis, centroidBox);
    if(strategy == BVH_Partition_Strategy::OBJECT_MEDIAN_SPLIT)
        return partitionObjectMedianSplit(intersectableInfo, infoLength, axis);
    if(strategy == BVH_Partition_Strategy::SURFACE_AREA_HEURISTIC)
        return partitionSurfaceAreaHeuristic(intersectableInfo, infoLength, axis, centroidBox);

    std::cerr << "BVH Partition strategy not supported.\n";
    exit(1);
}


