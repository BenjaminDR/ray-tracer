#include "AABoundingBox.h"
#include "Stats.h"

AABoundingBox::AABoundingBox() {
    maxCorner = Point(-DINF, -DINF, -DINF);
    minCorner = Point(DINF, DINF, DINF);
}

AABoundingBox::AABoundingBox(const Point &pMin, const Point &pMax) {
    minCorner = pMin;
    maxCorner = pMax;
}

AABoundingBox::AABoundingBox(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
    minCorner = Point(xmin, ymin, zmin);
    maxCorner = Point(xmax, ymax, zmax);
}

AABoundingBox::AABoundingBox(const AABoundingBox &box) {
    minCorner = box.minCorner;
    maxCorner = box.maxCorner;
}

unsigned AABoundingBox::getDominantAxis() const {
    Vector diagonal = maxCorner - minCorner;

    if(diagonal.x() >= diagonal.y() && diagonal.x() >= diagonal.z()) return 0;
    if(diagonal.y() >= diagonal.x() && diagonal.y() >= diagonal.z()) return 1;
    return 2;
}

bool AABoundingBox::intersect(const Ray &ray, HitObject &hitObject, double maxDist) const {
    STAT_OP(BBintersects_l++;)

    Vector invDir = ray.direction.cwiseInverse();

    double tMin = -DINF;
    double tMax = maxDist;

    for(unsigned axis = 0u; axis < 3; axis++) {
        double t1 = (maxCorner[axis] - ray.origin[axis]) * invDir[axis];
        double t2 = (minCorner[axis] - ray.origin[axis]) * invDir[axis];

        if(t1 > t2) std::swap(t1, t2);

        tMin = std::max(t1, tMin);
        tMax = std::min(t2, tMax);

        if(tMin > tMax) return false;
    }

    hitObject.distance = tMin > EPSILON ? tMin : std::min(tMax, 0.0);
    return true;
}

bool AABoundingBox::encloses(const Point &p) const {
    return (
        p.x() > minCorner.x() - EPSILON && p.x() < maxCorner.x() + EPSILON &&
        p.y() > minCorner.y() - EPSILON && p.y() < maxCorner.y() + EPSILON &&
        p.z() > minCorner.z() - EPSILON && p.z() < maxCorner.z() + EPSILON    );
}

double AABoundingBox::getMinForAxis(unsigned int axis) const {
    return minCorner[axis];
}

double AABoundingBox::getWidthForAxis(unsigned int axis) const {
    return maxCorner[axis] - minCorner[axis];
}

bool AABoundingBox::isZero() const {
    return minCorner == maxCorner;
}

double AABoundingBox::distanceTo(const Ray &ray) const {
    HitObject o;
    intersect(ray, o, DINF);
    return o.distance;
}

double AABoundingBox::recursiveSurfaceArea() const {
    return surfaceArea();
}

