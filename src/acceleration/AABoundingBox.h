#ifndef RAY_TRACER_AABOUNDINGBOX_H
#define RAY_TRACER_AABOUNDINGBOX_H


#include <vector>
#include <Base.h>
#include <math/Transformation.h>
#include "math/Ray.h"
#include "Intersectable.h"
#include "HitObject.h"

class AABoundingBox : Intersectable {

public:
    AABoundingBox();
    AABoundingBox(const Point &pMin, const Point &pMax);
    AABoundingBox(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);
    AABoundingBox(const AABoundingBox& box);

    bool intersect(const Ray &ray, HitObject &hitObject, double maxDist) const override;
    [[nodiscard]] double distanceTo(const Ray &ray) const override;
    [[nodiscard]] bool encloses(const Point &p) const override;
    [[nodiscard]] double recursiveSurfaceArea() const override;

    [[nodiscard]] unsigned getDominantAxis() const;

    [[nodiscard]] inline Point getCentroid() const {
        return (minCorner + maxCorner) / 2;
    }

    [[nodiscard]] inline double surfaceArea() const {
        return 2.0 * getHalfSurfaceArea();
    }

    [[nodiscard]] inline double getHalfSurfaceArea() const {
        Vector d = maxCorner - minCorner;
        return (d.x() * (d.y() + d.z()) + d.y() * d.z());
    }

    inline AABoundingBox& operator +=(const AABoundingBox &otherBox) {
        minCorner = minCorner.cwiseMin(otherBox.minCorner);
        maxCorner = maxCorner.cwiseMax(otherBox.maxCorner);
        return *this;
    }

    inline AABoundingBox& operator +=(const Vector &v) {
        minCorner = minCorner.cwiseMin(v);
        maxCorner = maxCorner.cwiseMax(v);
        return *this;
    }

    inline AABoundingBox operator +(const AABoundingBox &otherBox) const {
        return AABoundingBox(minCorner.cwiseMin(otherBox.minCorner), maxCorner.cwiseMax(otherBox.maxCorner));
    }

    inline AABoundingBox operator *(const Transformation &t) const {
        Point p1 = t.translatePoint(minCorner);
        Point p2 = t.translatePoint(maxCorner);
        return AABoundingBox(p1.cwiseMin(p2), p1.cwiseMax(p2));
    }

    [[nodiscard]] double getMinForAxis(unsigned axis) const;

    [[nodiscard]] double getWidthForAxis(unsigned axis) const;

    [[nodiscard]] bool isZero() const;

private:
    Point minCorner, maxCorner;
};




#endif //RAY_TRACER_AABOUNDINGBOX_H
