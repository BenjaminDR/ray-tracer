\documentclass{article}

\usepackage{algorithmicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsmath}
\usepackage[hidelinks]{hyperref}
\usepackage{graphicx}

\newcommand{\bigO}{\mathcal{O}}

\author{Benjamin De Roeck}
\title{Computer Graphics Project: Final report}

\begin{document}
\maketitle
\tableofcontents
\newpage

\section{Bounding Volume Hierarchy}

% kleine inleiding
Wanneer men complexere scènes wil renderen neemt het aantal primitieven in de scène snel toe.
Om niet voor elke ray alle primitieven te moeten doorlopen zal gekeken moeten worden naar datastructuren die toelaten om een aantal primitieven uit te sluiten op basis van vorige intersectietesten voor die ray.
Dit zorgt ervoor dat het renderen van een prentje opgesplitst wordt in een voorbereiding-stap waar deze data structuur opgebouwd wordt en een render-stap waar, gebruikmakend van deze acceleratiestructuur, het prentje berekend wordt.

%% Wat is/doet een BVH?
Twee veelgebruikte acceleratiestructuren zijn kd-trees en bounding volume hierarchies (BVH's).
Deze laatste werd in dit practicum geïmplementeerd en zal hier ook verder bestudeerd worden.
Een BVH probeert het aantal intersectietesten te beperken door een hierarchie op te bouwen van objecten die de primitieven omhullen. 
Als zo'n omhullend object niet geraakt wordt door een ray zullen de primitieven erbinnen ook niet geraakt kunnen worden.

%% Waarom AABB?
Vaak wordt er als omhullend object voor een as-gealigneerde balk (axis-aligned bounding box of AABB) gekozen vanwege de eenvoud om een omhullende AABB te berekenen voor veel objecten en omdat de intersectietest van een AABB zeer snel kan gebeuren. 
Omdat een AABB gezien kan worden als een doos begrensd door as-gealigneerde vlakken, is de intersectietest eigenlijk gewoon een serie van lijn-vlak intersecties.
Nog een voordeel aan het gebruik van een AABB is dat deze slechts met twee punten kan voorgesteld worden en de BVH zo maar een relatief kleine impact heeft op het gebruikte geheugen.
Hier staat wel tegenover dat een AABB niet altijd even nauw de objecten omsluit.
In dit practicum is een AABB gebruikt voor het opbouwen van een BVH.

%% Welke technieken gaan we bespreken?
Nu de soort acceleratiestructuur en het omsluitend object vast staan zijn er al veel factoren die inspelen op de kwaliteit van onze acceleratiestructuur weggevallen.
De BVH moet echter nog wel opbouwd worden en hiervoor kunnen meerdere strategieën gebruikt worden die beslissen hoe onze hiërarchie er uit ziet.
Er moet hier een afweging gemaakt worden tussen het snel opbouwen van de acceleratiestructuur en de kwaliteit van de structuur.
De strategieën, waar in dit practicum op gefocust werd, waren de object median split, spatial median split en surface area heuristic.

\subsection{Opbouwen van Bounding Volume Hierarchy}
% ultra-kleine inleiding
Om het probleem niet te verleggen naar de opbouw van onze acceleratiestructuur moeten we zien dat deze ook efficiënt kan worden opgebouwd.
Naast de kwaliteit van de BVH (hoeveel intersectietesten ermee bespaard kunnen worden), mag de complexiteit om deze op te stellen dus zeker niet worden genegeerd.
In dit practicum wordt de BVH van boven naar beneden opgebouwd (top-down construction), een zeer ruwe versie van hoe dit er uit kan zien is gegeven in algorithme \ref{alg:bvh_construction}.

\begin{algorithm}
    \caption{BVH construction step}
    \label{alg:bvh_construction}
\begin{algorithmic}
\Function{calcBvhStep}{$primitives[], Node$}
    \State $len \gets primitives.length()$
    \If{$len == 1$}
        \State $Node.primitive \gets primitives[0]$
        \State \Return
    \EndIf
    \For{prim in primitives}
        \State $Node.aabb \gets \Call{union}{Node.aabb, prim.box}$
    \EndFor
    \State $split \gets \Call{partition}{primitives, Node}$
    \State $ \Call{calcBvhStep}{primitives[0:split], Node.left}$
    \State $ \Call{calcBvhStep}{primitives[split:end], Node.right}$
\EndFunction
\end{algorithmic}
\end{algorithm}

Uiteraard kan dit nog enorm geoptimaliseerd worden.
Men kan het aanmaken van nieuwe lijsten vermijden, de recursieve methode inwisselen voor een iteratieve methode, sommige aftakkingen door andere threads laten berkenen, rekening houden met de cache layout,...
Om onderstaande resultaten te bekomen zijn een aantal optimalisaties gebruikt.

De \textit{partition} functie zal de elementen van de lijst herordenen en een index teruggeven waar de lijsten verdeeld worden.
In dit verslag zullen drie strategieën bekeken worden om dit te doen, deze worden hier kort besproken met een kleine vergelijking.

\subsubsection{Object median split}
De object median split is een hele makkelijke strategie om een BVH top-down te construeren.
We bekijken de midden-coördinaten voor een bepaalde as en zoeken hier de mediaan.
Vervolgens partitioneren we de primitieven zo dat alle kleinere en grotere midden-coördinaten op die as respectievelijk links en rechts van deze mediaan liggen.
We nemen het splitsingsvlak bij deze mediaan zodat evenveel primitieven links, als rechts van het vlak liggen (op basis van hun middelpunt).
De partitie-methode is weergegeven in algortime \ref{alg:object_median_split}.

\begin{algorithm}
    \caption{Object median split}
    \label{alg:object_median_split}
\begin{algorithmic}
\Function{partitionObjectMedian}{$primitives[], Node$}
    \For{$prim in primitives$}
        \State $centroidBox \gets \Call{union}{centroidBox, prim.aabb.centroid}$
    \EndFor
    \State $ axis \gets \Call{getSplittingAxis}{centroidBox}$
\State $ pivot \gets \Call{findMedian}{primitives, axis}$
    \State $ \Call{partition}{primitives, pivot[axis], axis}$
    \State \Return $(0 + primitives.length()) / 2$
\EndFunction
\end{algorithmic}
\end{algorithm}

Vervolgens zal de complexiteit van het bouwen van een BVH met de object median split hier bekeken worden.
Het opbouwen van de $centroidBox$ kan met $\bigO(N)$ vergelijkingen.
We gaan er hier voorlopig van uit dat de as met een constant aantal vergelijkingen gekozen wordt. 
De mediaan kan gevonden worden met $\bigO(N)$ vergelijkingen.
Ten slot partitioneren we de primitieven wat eveneens $\bigO(N)$ vergelijkingen vergt.
De $partitionObjectMedian$ methode heeft dus een complexiteit van $\bigO(N)$.

Voor het recursieve algoritme \ref{alg:bvh_construction} komt dit uit op
\begin{align*}
    \bigO(N + \frac{N}{2} + \frac{N}{4} + ... + 2) = \bigO(N \cdot log(N))
\end{align*}
We maken hier gebruik van de eigenschap dat de object median split het aantal primitieven per recursie-stap steeds in twee zal delen.
De BVH zal dus opgebouwd worden met een complexiteit van $\bigO(N \cdot log(N))$.
\subsubsection{Spatial median split}
De spatial median gaat vrij gelijkaardig tewerk maar maakt een andere keuze voor de pivot.
Om deze te kiezen wordt het midden-coördinaat van de AABB die de midden-coördinaten van de primitieven omsluit berekend, deze wordt dan gebruikt als pivot.
De lijst van primitieven wordt nu gepartitioneerd op basis van de ligging van hun midden-coördinaat tegenover de pivot, volgens een bepaalde as.
De partitie-methode is weergegeven in algoritme \ref{alg:spatial_median_split}.

\begin{algorithm}
    \caption{Spatial median split}
    \label{alg:spatial_median_split}
\begin{algorithmic}
\Function{partitionSpatialMedian}{$primitives[], Node$}
    \For{$prim in primitives$}
        \State $centroidBox \gets \Call{union}{centroidBox, prim.aabb.centroid}$
    \EndFor
    \State $ axis \gets \Call{getSplittingAxis}{centroidBox}$
    \State $ pivot \gets centroidBox.centroid$
    \State $ splitIndex \gets \Call{partition}{primitives, pivot[axis], axis}$
    \State \Return $ splitIndex $
\EndFunction
\end{algorithmic}
\end{algorithm}
De verschillende stappen hebben dezelfde complexiteit als in algoritme \ref{alg:object_median_split}.
Om nu ook de complexiteit van het recursieve algoritme \ref{alg:bvh_construction} uit te werken moet het probleem omzeild worden dat niet geweten is hoeveel primitieven in de linker/rechter AABB zullen zitten.
Alternatief kan eenvoudiger een best- en worst-case complexiteit berekend worden.
In het beste geval is het aantal primitieven in de linker AABB gelijk aan die in de rechter, de berekening van de tijdscomplexiteit in dit geval is dus dezelfde als bij de object median split.
\begin{align*}
    \bigO(N + \frac{N}{2} + \frac{N}{4} + ... + 2) = \bigO(N \cdot log(N))
\end{align*}
In het slechtste geval zal in de linker of rechter AABB slechts één primitief zitten bij elke recursie-stap.
Dit geeft de volgende tijdscomplexiteit.
\begin{align*}
    \bigO(N + (N-1) + (N-2) + ... + 2) &= \bigO(\frac{(N-1)(N+2)}{2}) \\
                                       &= \bigO(\frac{N^2}{2} + \frac{N}{2} - 1) \\
                                       &= \bigO(N^2) \\
\end{align*}
Men kan dus inzien dat de tijdscomplexiteit voor een scene te bouwen kan variëren van $\bigO(N^2)$ in het slechtste (hoogst weinig voorkomende geval) tot een complexiteit van $\bigO(N \cdot log(N))$ in het beste geval. 

\subsubsection{Surface area heuristic}
De surface area heuristic is een techniek die sterk lijkt op de spatial median split, maar uit meerdere ruimtelijke posities kiest om de primitieven in twee groepen te splitsen.
Die beslissing wordt gemaakt aan de hand van een bepaalde heuristiek die geëvalueerd wordt voor al deze posities.
In dit practicum is er gekozen om de "binned" variatie van de surface area heuristic te implementeren, deze beschouwt slecht $B - 1$ equidistante splitsingsvlakken met $B$ het aantal bins.
De complexiteit van dit algoritme kan gelijkaardig aan dat van de spatial median split berekend worden.
Er gebeuren wel een aantal extra stappen zoals het tellen van het aantal primitieven in elke "bin" en het $B - 1$ keer evalueren van de heuristiek waarbij ook telkens de unie van een aantal bins moet worden genomen.
Voor een constant aantal bins kunnen al deze stappen gebeuren in $\bigO(N)$ waardoor de best-case tijdscomplexiteit nog steeds $\bigO(N \cdot log(N))$ blijft.
Ook de worst-case complexiteit zal $\bigO(N^2)$ zijn, het geval dat er steeds slechts 1 primitief afgezonderd wordt is nog steeds mogelijk, al nog zeldzamer.
Er is reeds veel onderzoek uitgevoerd naar optimale bin-groottes voor bepaalde scènes, er is hier gekozen om met één bin-grootte van $B = 16$ te werken.

\subsubsection{Complexiteit analyse}

\begin{figure}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/new/build-complexity-regular.png}
    \end{center}
    \caption{Opbouwen BVH, reguliere spreiding}
    \label{fig:regular-build}
\end{figure}

Alle drie bouw-strategiën hebben een verwachte complexiteit van $\bigO(N \cdot log(N))$, voor de surface area heuristic en spatial median split is dit slechts een best-case complexiteit en zal deze meer richting $\bigO(N^2)$ neigen.
Om deze theoretische complexiteit te controleren, meten we de bouw-tijd van de BVH voor elke methode voor een bepaalde scène.
We maken gebruik van de scene-generator (zie sectie \ref{sec:scene-generator}) om scènes te genereren van verschillende distributies.
Als de objecten in de scène regulier verdeeld zijn zal voor de spatial median split en de surface area heuristic telkens een splitsing gekozen worden die de objecten perfect in twee verdeeld.
Voor de uniforme verdeling zal deze verdeling ook vrij goed zijn.
Bij de normale verdeling zullen de primitieven echter schever verdeeld zijn over de ruimte en zullen het splitsingsvlak de primitieven gewoonlijk in minder gelijke groepen verdelen.

Alle testen werden uitgevoerd op een Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz en het gemiddelde werd genomen van 100 uitvoeringen. Als primitief is hier een bol gekozen.

In figuur \ref{fig:regular-build} zien we een log-log plot van de bouwtijden voor de regulier spreiding van primitieven.
We merken een rechte op met richtingscoëfficiënt $ r \approx 1.2$.
De functie $ a * N^{1.2} $ kan echter vrij gelijk lopen met een functie $b * N * log(N)$ tot zeer grote waarden voor $N$.
Het zou dus kunnen dat deze reden afvlakt voor hogere $N$ wat overeen zou komen met de theoretische complexiteit.
Door een beperkt computergeheugen kan ik dit echter niet simuleren.

\begin{figure}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/new/build-complexity-SMS.png}
    \end{center}
    \caption{Opbouwen BVH, Spatial median split}
    \label{fig:SMS-build}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/new/build-complexity-SAH.png}
    \end{center}
    \caption{Opbouwen BVH, Surface area heuristic}
    \label{fig:SAH-build}
\end{figure}

Vervolgens vergelijken we de bouwtijden voor de spatial median split en surface area heuristic over schillende distributies in figuren \ref{fig:SMS-build} en \ref{fig:SAH-build}.

Het valt op dat de scheve ruimtelijke verdeling van de normale distributie inderdaad een kleine negatieve invloed heeft op de bouwtijd van de BVH's bij deze methodes.
Dit is echter nog ver van de theoretische worst-case complexiteit.
Ook is op te merken dat dit effect minder prominent is bij de surface area heuristic, dit is niet verassend aangezien deze methode keuze laat aan meerdere splitsingsvlakken en niet enkel de meest centrale.


\subsection{Kwaliteit Bounding Volume Hierarchy}
 Natuurlijk is het zeer belangrijk dat een BVH kwaliteitsvol is.
 Hier wordt het woord kwaliteitsvol gebruikt als een maatstaaf voor de vermindering in aantal intersectietesten nodig om de scène te renderen.
 Meer concreet wordt bedoeld dat een BVH meer kwaliteitsvol is als deze over het algemeen (onafhankelijk van de de camera positie/oriëntatie) minder intersectietesten nodig heeft om te renderen.
 Een manier om dit te meten is door onze scène vanuit $N$ willekeurige camera posities/oriëntaties te renderen, met en zonder de BVH.
 Vervolgens kan het verschil genomen worden van het aantal intersectietesten nodig met en zonder de BVH en hier een gemiddelde van berekend worden.
 Voor stijgende $N$ zal dit convergeren naar een goede maatstaaf voor de kwaliteit van de BVH.

Uiteraard is dit een hele dure manier om de kwaliteit van een BVH te meten.
Een alternatieve manier om een idee van de kwaliteit te krijgen zonder de scène een heleboel keer te moeten renderen is door te berekenen hoeveel intersectietesten er gemiddeld uitgevoerd zullen worden voor elke straal.
Dit kan gedaan worden door de kostfunctie te implementeren die in het vak \textit{Fundamenten van computergrafiek (G0Q66C)} gezien werd, deze is weergegeven in formule \ref{eq:cost-1}.
Hier wordt de kost voor een knoop $parent$ in de BVH berekend, $L$ en $R$ zijn haar twee kind-knopen, $C_t$ is de kost om de knoop te doorkruisen (kost voor een intersectietest) en de $prob$ functie berekent de kans opdat een straal ook een kind-knoop zal doorkuisen.
\begin{equation}
    \label{eq:cost-1}
    Cost(parent) = C_t + prob(hit_L) \cdot Cost(L) + prob(hit_R) \cdot Cost(R)
\end{equation}
Om de kost voor te stellen als het gemiddeld aantal intersecties nodig voor een straal die $Parent$ doorkruist, kan $C_t$ gelijkgesteld worden aan 1.
Ook kan de $prob$ functie verder uitwerken worden aangezien $prob(hit_n) = S_n / S_{parent}$ met $S_n$ de oppervlake van Node $n$.

Dit resulteert in formule \ref{eq:cost-2}.
\begin{equation}
    \label{eq:cost-2}
    Cost(parent) = 1 + \frac{S_L}{S_{parent}} \cdot Cost(L) + \frac{S_R}{S_{parent}} \cdot Cost(R)
\end{equation}

\subsubsection{Object median split}
Vanwege de recursieve oproep in formule \ref{eq:cost-1} is het niet mogelijk deze te evalueren voor de BVH volledig is opgesteld.
Dit kan opgelost worden door te werken met een ruwe benadering van de kostformule, in dit verslag wordt gebruikt gemaakt van de schatting $Cost(n) \approx N_n \cdot C_p$ waar $N_n$ het aantal primitieven gelegen binnen $n$ voorstelt en $C_p$ de kost voor een intersectietest van een primitief. 
Voor een kwaliteitsvolle BVH is dit meestal een overschatting, maar dat is niet altijd het geval.
Uit formule \ref{eq:cost-1} volgt dan formule \ref{eq:cost-3}.
\begin{equation}
    \label{eq:cost-3}
    Cost(parent) = C_t + prob(hit_L) \cdot N_L \cdot C_p + prob(hit_R) \cdot N_R \cdot C_p
\end{equation}
Omdat bij de object median split het splitsingvlak zo genomen wordt dat er evenveel primitieven links als rechts liggen, zal hier $N_L = N_R = N_{parent} / 2$ als er vanuit gegaan wordt dat we met een even aantal primitieven werken.
Formule \ref{eq:cost-3} kan dan verder vereenvoudigd worden tot formule \ref{eq:cost-oms}.
\begin{equation}
    \label{eq:cost-oms}
    Cost(parent) = C_t + 0.5 \cdot N_{parent} \cdot C_p \cdot (prob(hit_L) + prob(hit_R))
\end{equation}
Hier is de enige onbekende factor de som $prob(hit_L) + prob(hit_R)$.
Opdat de kost laag is moet deze som zo klein mogelijk zijn.
Dit is des te meer belangrijk hogerop in de BVH aangezien de $N_{parent}$ factor steeds met een factor 2 groter wordt van kind-knoop naar ouder-knoop.
Aangezien, in een BVH, knopen kunnen overlappen, kan deze som ook groter worden dan 1.
In het geval dat de oppervlakte van $L$ en $R$ minimaal is voor dit splitsingsvlak zal de object median split de knoop optimaal splitsen.
Op basis van deze informatie is het kan de hypothese gemaakt worden dat de Object median split strategie redelijk goed zal presteren bij scènes met willekeurig geplaatse primitieven en minder goed bij scènes waar een groot deel van de primitieven bijeengeclusterd over een klein volume.
%% todo: uitleggen waarom en zien of dit wel klopt!!

\subsubsection{Spatial median split}
Zoals bij de object median split de motivatie achter de strategie ligt in het zo goed mogelijk verdelen van het aantal primitieven, gaat de spatial median split zich focussen op de andere factor die aan onze kostfunctie bijdraagt, de kans op intersectie van een kind-knoop.
De spatial median split zal dus trachten zo goed mogelijk de ruimte te verdelen om deze kans niet te groot te maken voor één van de kind-knopen.
Aangezien knopen kunnen overlappen is het zeker dat $prob(hit_L) + prob(hit_R)$ niet minimaal is als de ruimte perfect in 2 delen verdeeld wordt.
Toch is dit de beste gok die kan worden gemaakt zonder meerdere posities te testen.

Dat het aantal primitieven per kind-knoop goed verdeeld is, is niet gegarandeerd bij de spatial median split.
Dit kan voor situaties zorgen waar consequent een groot percentage van het aantal primitieven aan één kant van het splitsingsvlak terechtkomt wat er op zijn beurt voor zorgt dat de BVH boom zeer scheef zal zijn.
Dergelijke scheve boom heeft als nadeel dat rays nabij de dieper gelegen primitieven zeer veel intersectietesten nodig hebben om intersectie na te gaan.
Deze scheefheid is vooral nadelig bovenaan in de boom aangezien daar meer primitieven er invloed van ondervinden.

Op basis van deze informatie kan de hypothese gevormd worden dat de Spatial median split redelijk goed zal presteren bij scènes waar primitieven ongeveer gelijk over de ruimte verspreid zijn en wat minder goed bij scènes waar een groot deel van de primitieven bijeengeclusterd zijn over een klein volume.
%% todo: uitleggen waarom
%% todo: uitleggen waarom zelfde als object median split
\subsubsection{Surface area heuristic}
De surface area heuristic strategie probeert zowel rekening te houden met de oppervlakte van de kind-knopen te minimaliseren als met de hoeveelheid primitieven per kind-knoop goed te verdelen.
Dit gebeurt door formule \ref{eq:cost-3} uit te rekenen voor alle kandidaat-splitsingsvlakken.
Er wordt vervolgens gekozen voor het splitsingsvlak waarvoor de kleinste kost is geschat.
Aangezien er in dit practicum gewerkt is met de binned-aproach zal het aantal kandidaat-splitsingsvlakken gelijk zijn aan $N_{bins} - 1$ met $N_{bins}$ het aantal bins dat we hebben gekozen.
Interessant aan de surface area heuristiek is dat als het aantal bins gelijk genomen wordt aan 2 (met dus 1 kandidaat-splitsingsvlak) is deze strategie gelijk aan de spatial medium split.

Aangezien deze strategie meer factoren in rekening brengt en meerdere mogelijke splitsingsvlakken in overweging neemt kan verwacht worden dat deze over het algemeen betere resultaten zal opleveren in vergelijking met de object median split en de spatial median split.
Dit is echter geen vereiste aangezien formule \ref{eq:cost-3} maar een schatting is en kan afwijken van de werkelijke kost.
Ook kan over het algemeen verwacht worden dat het vergroten van het aantal bins de kwaliteit van de BVH positief zal beïnvloeden aangezien de heuristiek zo op meer plaatsen geëvaluereerd wordt en dus over een breder assortiment aan kandidaten gekozen wordt.
Wederom zal dit zeker niet altijd het geval zijn aangezien de heuristiek maar een schatting is van de werkelijke kost.
\subsubsection{Vergelijking}

Om de performantie van de datastructuren deterministisch te vergelijken zullen we de scène voor elke bvh-strategie en verdeling renderen en bijhouden hoeveel intersectietesten er gebruikt werden.
We bekijken dan het aantal intersectietesten per ray om een idee te hebben van de uitvoertijd voor het berekenen van een prentje.
Voor de volgende plots is een logaritmische schaal gebruikt voor de x-as om een beter idee te hebben van de complexiteit.

\begin{figure}
    \begin{center}
    \includegraphics[width=1\textwidth]{figures/new/render-regular.png}
    \end{center}
    \caption{Aantal intersectie-testen per ray voor een reguliere spreiding}
    \label{fig:regular-render}
\end{figure}

In figuur \ref{fig:regular-render} zien we een rechte voor de drie methodes, aangezien de x-as een logaritmische schaal heeft zal het aantal intersectie-testen per ray zich ook logaritmisch voortbrengen.
Dit zorgt voor een $\bigO(R \cdot log(N))$ complexiteit voor het renderen van een scène met $N$ primitieven en $R$ rays.

Wat opvalt is dat het aantal intersectie-testen per ray sneller stijgt voor de object median split.
Dit gaat in tegen mijn verwachtingen en ik heb hier op dit moment nog geen verklaring voor gevonden.

\begin{figure}
    \begin{center}
    \includegraphics[width=1\textwidth]{figures/new/render-uniform.png}
    \end{center}
    \caption{Aantal intersecties per ray voor een uniform verdeelde scène}
    \label{fig:uniform-render}
\end{figure}

In figuur \ref{fig:uniform-render} zien we opnieuw dat het aantal intersectie-testen per ray logaritmisch stijgt met het aantal primitieven.
Hier zien we wel dat de drie methodes vrij goed overeenkomen.

\begin{figure}
    \begin{center}
    \includegraphics[width=1\textwidth]{figures/new/render-normal.png}
    \end{center}
    \caption{Aantal intersecties-testen per ray voor een normaal verdeelde scène}
    \label{fig:normal-render}
\end{figure}

In figuur \ref{fig:normal-render} kan worden gezien dat het aantal nodige intersectietesten per ray sneller dan logaritmisch stijgt.
Dit is vooral zichtbaar bij de object median split.
Een mogelijke verklaring hiervoor is dat bij de object median split vanaf tweede recursie-niveau zeer grote bounding boxes zal hebben met veel lege ruimte in.

\begin{figure}
    \begin{center}
    \includegraphics[width=1\textwidth]{figures/new/render-OMS.png}
    \end{center}
    \caption{Aantal intersectie-testen per ray voor object median split }
    \label{fig:OMS-render}
\end{figure}
\begin{figure}
    \begin{center}
    \includegraphics[width=1\textwidth]{figures/new/render-SMS.png}
    \end{center}
    \caption{Aantal intersectie-testen per ray voor spatial median split}
    \label{fig:SMS-render}
\end{figure}
\begin{figure}
    \begin{center}
    \includegraphics[width=1\textwidth]{figures/new/render-SAH.png}
    \end{center}
    \caption{Aantal intersectie-testen per ray voor surface area heuristic}
    \label{fig:SAH-render}
\end{figure}

In figuren \ref{fig:OMS-render}, \ref{fig:SMS-render} en \ref{fig:SAH-render} zien we de vorige resultaten nog eens vergeleken per bvh-constructie methode.

Het valt hier nog extra op dat de reguliere spreiding optimaal is voor de spatial median split en de surface area heuristic. Bij de uniforme distributie zien we een gelijkaardig verloop maar met afgezien van een kleine factor.

De normale distributie lijkt over de drie methodes meer intersectie-testen nodig te hebben voor een groot aantal primitieven.
Na het produceren van deze figuren merkte ik echter dat er een fout zat op de meting van de intersectietesten voor de normale distributie.
De willekeurig gegenereerde camera positie staat hier verder weg van de primitieven waardoor gemiddeld minder stralen de scène raken.

\subsection{Keuze van de as}
Voor het produceren van bovenstaande resultaten is gekozen om steeds de meest dominante as als splitsings-as te gebruiken.
Andere strategieën zijn zeker mogelijk. In mijn implementatie zit ook de optie om sequentieel de assen af te gaan of om de splitsing uit te proberen voor elke as en vervolgens de beste te kiezen.

\section{Object Instancing}
Object instancing is een nuttige techniek om geheugen te besparen bij scènes die een bepaald object meerdere keren gebruiken met verschillende transformaties.
Deze techniek is volledig geïmplementeerd in dit project alsook een alternatieve strategie om alle driehoeken vooraf te transformeren en in één mesh te verzamelen.

Uit een onderzoek naar het geheugengebruik van de raytracer kwamen volgende resultaten.

\begin{figure}
    \begin{center}
    \includegraphics[width=1\textwidth]{figures/new/memory-instancing.png}
    \end{center}
    \caption{Geheugen gebruik in functie van het aantal Venus-meshes}
    \label{fig:memory-instancing}
\end{figure}
\begin{figure}
    \begin{center}
    \includegraphics[width=1\textwidth]{figures/new/memory-flat.png}
    \end{center}
    \caption{Geheugengebruik in functie van het aantal Venus-meshes}
    \label{fig:memory-flat}
\end{figure}

In figuur \ref{fig:memory-instancing} kunnen we zien dat het geheugengebruik toeneemt met een constante van enkele kilobytes.
In figuur \ref{fig:memory-flat} neemt het geheugengebruik ook toe met een constante, deze is echter afhankelijk van de grootte van de mesh en een stuk groter.

\section{Scene generator}
\label{sec:scene-generator}

Bij het evalueren van acceleratiestructuren is het handig om fijnere controle te hebben over zaken zoals
\begin{itemize}
    \item de hoeveelheid primitieven in de scène
    \item de ruimtelijke verdeling van deze primitieven over de scène
    \item de onderlinge groottes van de primitieven
    \item de kijkhoek van de camera
\end{itemize}

Om dit te bereiken is in dit project een generator geschreven die scenes kan genereren gegeven volgende parameters:
\begin{itemize}
    \item de vorm van het primitief of mesh
    \item het aantal gewenste primitieven/meshes
    \item (kansverdeling) voor spreiding (uniform verspreid, uniform verdeeld, normaal verdeeld, exponentiëel verdeeld)
    \item noise-ratio voor grootte (voor een ratio van nul zijn alle primitieven of meshes even groot)
\end{itemize}
Voor elke scène kan de generator ook willekeurige camera-posities en oriëntaties genereren waarvoor de scène volledig zichtbaar is.

Het kan ook fractalen genereren tot een gegeven recursie-diepte.
Op dit moment is enkel de sphereflake geïmplementeerd.
Enkele voorbeelden zijn hieronder te vinden.

\begin{figure}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/generator-uni-10000.png}
    \end{center}
    \caption{10000 bollen uniform verspreid, scale-noise-ratio 0}
    \label{fig:gen-uni}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/generator-uni-dist-10000.png}
    \end{center}
    \caption{10000 bollen uniform verdeeld, scale-noise-ratio 1}
    \label{fig:gen-uni-dist}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/generator-norm-1000-sah.png}
    \end{center}
    \caption{1000 Venus meshes, normaal verdeeld, scale-noise-ratio 2}
    \label{fig:gen-norm}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/generator-exp-1000-sms.png}
    \end{center}
    \caption{1000 Venus meshes, exponentiëel verdeeld, scale-noise-ratio 0}
    \label{fig:gen-exp}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/sphereflake.png}
    \end{center}
    \caption{Sphereflake fractaal, recursie-diepte 4}
    \label{fig:sphereflake}
\end{figure}

\end{document}
