# Ray Tracer

A Ray Tracer written in C++ with a main focus on performance and the use of acceleration structures.

## Feedback

I don't believe many eyes will see this code except my own but if you do and have some tips that could improve this project or my knowledge of C++, please feel free to leave any feedback.

## How to run

The project can be installed by running following commands.

```bash
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
cmake -DCMAKE_BUILD_TYPE=Release ..
```
The project can then be recompiled with the following commands.

```bash
cd build
make
```

## Debugging

When debugging with gdb, it can be handy to pretty print variables of the Euler library, this can be done by creating a file `.gdbinit` into the project directory with the following contents.
```
python
import sys
sys.path.insert(0, '/path/to/project/gdb/auto-load/')
from printers import register_eigen_printers
register_eigen_printers (None)
end
```

Obviously you should update the path.

Then you can add the following lines to your `~/.gdbinit` file:
```
set auto-load local-gdbinit on
add-auto-load-safe-path /path/to/project/.gdbinit
```
with once again, the path updated.
